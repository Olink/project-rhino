import os

folders = ["CommonUtilities", "DummyClient", "ProjectRhino"]
files = []
count = {}

for project in folders:
    for dir in os.walk(project):
        root = dir[0]
        for file in dir[2]:
            if file.endswith(".cs"):
                files.append(os.path.join(root, file))

for file in files:
    with open(file, 'r') as f:
        lines = f.readlines()

    lines = [x for x in lines if len(x.strip()) > 0]

    count[file] = len(lines)

total = 0
for k,v in count.items():
    total += v

count["total"] = total

print(count)
print(count['total'])
