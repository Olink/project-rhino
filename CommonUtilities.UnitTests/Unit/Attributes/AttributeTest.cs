﻿using CommonUtilities.Attributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Attribute = CommonUtilities.Attributes.Attribute;

namespace CommonUtilities.UnitTests.Unit.Attributes
{
	[TestClass]
	public class AttributeTest
	{
		[TestMethod]
		public void RawValueBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new RawAttributeBonus(10, 0));

			Assert.AreEqual(20, dex.CalculateValue());
		}

		[TestMethod]
		public void RawPercentBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new RawAttributeBonus(0, 0.5f));

			Assert.AreEqual(15, dex.CalculateValue());
		}

		[TestMethod]
		public void RawMixedBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new RawAttributeBonus(10, 1f));

			Assert.AreEqual(40, dex.CalculateValue());
		}

		[TestMethod]
		public void FinalValueBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new FinalAttributeBonus(10, 0));

			Assert.AreEqual(20, dex.CalculateValue());
		}

		[TestMethod]
		public void FinalPercentBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new FinalAttributeBonus(0, 0.5f));

			Assert.AreEqual(15, dex.CalculateValue());
		}

		[TestMethod]
		public void FinalMixedBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new FinalAttributeBonus(10, .5f));

			Assert.AreEqual(30, dex.CalculateValue());
		}

		[TestMethod]
		public void RawFinalValueBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new RawAttributeBonus(10, 0));
			dex.AddAttributeBonus(new FinalAttributeBonus(10, 0));

			Assert.AreEqual(30, dex.CalculateValue());
		}

		[TestMethod]
		public void RawFinalPercentBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new RawAttributeBonus(0, .5f));
			dex.AddAttributeBonus(new FinalAttributeBonus(0, 1f));

			Assert.AreEqual(30, dex.CalculateValue());
		}

		[TestMethod]
		public void RawFinalMixedBonus()
		{
			Attribute dex = new Attribute(10, 0);

			dex.AddAttributeBonus(new RawAttributeBonus(10, .5f));
			dex.AddAttributeBonus(new FinalAttributeBonus(10, 1f));

			Assert.AreEqual(80, dex.CalculateValue());
		}
	}
}
