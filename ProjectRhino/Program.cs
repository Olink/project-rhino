﻿using System;
using CommonUtilities;
using CommonUtilities.Messages;
using Ninject;
using Ninject.Parameters;

namespace ProjectRhino
{
    class Program
    {
        public static int ServerPort = 27010;
        public static readonly IKernel Container = new StandardKernel();
        Logger m_Logger;

        public Program()
        {
            m_Logger = new Logger("log.txt");
            m_Logger.SetLogLevel(31);

            Container.Bind<IServerMessageSerializer>().To(typeof(ServerMessageSerializer));
            Container.Bind<IClientMessageSerializer>().To(typeof(ClientMessageSerializer));
            Container.Bind<Logger>().ToConstant(m_Logger);

            Container.Bind<MasterServer>().To(typeof(MasterServer));

            var server = Container.Get<MasterServer>(new ConstructorArgument("container", Container), new ConstructorArgument("port", ServerPort));

            string input = string.Empty;
            do
            {
                input = Console.ReadLine();

                var parameters = Utilities.BreakIntoParameters(input);
                if (parameters.Length > 0)
                {
                }

            } while (input != string.Empty);

            server.Shutdown();
        }

        static void Main(string[] args)
        {
			if (args.Length > 0)
			{
				int port = 0;
				if (Int32.TryParse(args[0], out port))
					ServerPort = port;
			}
			new Program();
            
			/*var b = new Battle.Battle();
			b.AddCombatent(new AIPlayer(5, "Steve"));
			b.AddCombatent(new AIPlayer(5, "George"));
			b.StartBattle();
			Console.ReadKey();*/
		}
    }
}
