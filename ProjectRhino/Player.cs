﻿using System.Collections.Generic;
using System.Linq;
using CommonUtilities.Item;
using CommonUtilities.Messages;
using CommonUtilities.Players;
using ProjectRhino.UserAccounts;

namespace ProjectRhino
{
	public class Player : IPlayer
	{
		public ConnectionState State { get; set; }
		private IMessenger Messenger { get; set; }
		public IUser User { get; set; }
		public string UUID { get; set; }
		public string Name { get; set; }
		public bool InBattle { get; set; }
		public string BattleUUID { get; set; }

		public IReceiver Connection { get; set; }

		public bool CurrentTurn { get; set; }

		public Player(IReceiver conn, IMessenger mess)
		{
			Connection = conn;
			Messenger = mess;
		}

		public void EquipItem(int itemId, int unitId, EquipmentSlot slot)
		{
			var item = User.Inventory.Contents[itemId];
			User.Inventory.RemoveItem(item);
			var removedItems = new List<IEquipment>();
			if (item is Weapon)
				removedItems = User.Roster.Units[unitId].EquipItem((Weapon)item, slot);
			else if (item is EmptyEquipment)
				removedItems = User.Roster.Units[unitId].EquipItem((EmptyEquipment)item, slot);
			else if (item is Armor)
				removedItems = User.Roster.Units[unitId].EquipItem((Armor)item, slot);

			SendMessage(new CommonUtilities.Messages.Server.InventorySlot(itemId, new EmptyEquipment(slot)));

			foreach (var it in removedItems.Where(i => !(i is EmptyEquipment)))
			{
				int index = User.Inventory.AddItem(it);
				SendMessage(new CommonUtilities.Messages.Server.InventorySlot(index, it));
			}

			SendMessage(new CommonUtilities.Messages.Server.UnitUpdate(unitId, User.Roster.Units[unitId]));
		}

		public void RemoveItem(int unitId, EquipmentSlot slot)
		{
			var item = User.Roster.Units[unitId].RemoveItem(slot);
			if (item is EmptyEquipment)
				return;

			int index = User.Inventory.AddItem(item);
			SendMessage(new CommonUtilities.Messages.Server.InventorySlot(index, item));
			SendMessage(new CommonUtilities.Messages.Server.UnitUpdate(unitId, User.Roster.Units[unitId]));
		}

		public void SendMessage(IMessage packet)
		{
			Messenger.SendMessage(packet, Connection);
		}

		public bool Equals(Player ply)
		{
			return ply.Connection.Equals(Connection);
		}
	}

	public enum ConnectionState
	{
		None,
		Connected,
		LoggedIn
	}
}
