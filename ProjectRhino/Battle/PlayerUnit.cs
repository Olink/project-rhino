﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonUtilities.Unit;

namespace ProjectRhino.Battle
{
	class PlayerUnit : IUnit
	{
		private Unit unit;
		private int currentHP = 0;
		private int currentMP = 0;
		private bool alive = true;
		private int charge = 0;

		public int MaxHP
		{
			get { return (int)unit.Attributes.HP.CalculateValue(); }
		}

		public int CurrentHP
		{
			get { return currentHP; }
		}

		public int MaxMP
		{
			get { return (int)unit.Attributes.MP.CalculateValue(); }
		}

		public int CurrentMP
		{
			get { return currentMP; }
		}

		public bool Alive
		{
			get { return alive; }
		}

		public int CurrentCharge
		{
			get { return charge; }
		}

		public string Name { get; private set; }

		public PlayerUnit(Unit unit)
		{
			this.unit = unit;
			currentHP = MaxHP;
			currentMP = MaxMP;
		}

		public int Charge()
		{
			return charge += (int)unit.Attributes.Agility.CalculateValue();
		}

		public void PhysicalAttack(int damage)
		{
		}

		public int GetPhysicalDamage()
		{
			return 0;
		}

		public int GetDexterity()
		{
			return 0;
		}

		public void SpendCharge(int i)
		{
		}
	}
}
