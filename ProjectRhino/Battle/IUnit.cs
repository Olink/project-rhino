﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonUtilities.Unit;

namespace ProjectRhino.Battle
{
	interface IUnit
	{
		int MaxHP { get; }
		int CurrentHP { get; }
		int MaxMP { get; }
		int CurrentMP { get; }
		bool Alive { get; }
		int CurrentCharge { get; }
		String Name { get; }

		int Charge();
		void PhysicalAttack(int damage);
		int GetPhysicalDamage();
		int GetDexterity();
		void SpendCharge(int i);
	}
}
