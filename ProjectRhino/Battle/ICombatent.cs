﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ProjectRhino.Battle
{
	interface ICombatent
	{
		string Name { get; }
		List<IUnit> Units { get; }

		void StartBattle(Battle currentBattle, List<ICombatent> otherPlayers);
		void BeginTurn(IUnit activeUnit);
		void EndTurn();
	}
}
