﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CommonUtilities.Unit;

namespace ProjectRhino.Battle
{
	class AICombatent : ICombatent
	{
		private static int count = 0;

		private List<IUnit> units;
		private List<ICombatent> otherPlayers;
		private Battle currentBattle;
		private string name;

		public string Name
		{
			get { return name; }
		}

		public List<IUnit> Units
		{
			get { return units; }
		}

		public AICombatent()
		{
			name = "AI" + count++;
			units = new List<IUnit>();
			for (int i = 0; i < 4; ++i)
			{
				units.Add(new Monster(name));
			}
		}

		public void StartBattle(Battle currentBattle, List<ICombatent> otherPlayers)
		{
			this.otherPlayers = otherPlayers;
			this.currentBattle = currentBattle;
		}

		public void BeginTurn(IUnit activeUnit)
		{
			IUnit unitToTarget = null;
			foreach (var player in otherPlayers)
			{
				foreach (var unit in player.Units)
				{
					if (unit.Alive)
					{
						if (unitToTarget == null || unitToTarget.CurrentHP > unit.CurrentHP)
						{
							unitToTarget = unit;
						}
					}
				}
			}
			currentBattle.PerformAttack(activeUnit, unitToTarget);
			currentBattle.EndTurn(activeUnit);
		}

		public void EndTurn()
		{
		}
	}
}
