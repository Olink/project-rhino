﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectRhino.Battle
{
	class PlayerCombatent : ICombatent
	{
		private Player player;
		private List<IUnit> units;
 
		public string Name
		{
			get { return player.Name; }
		}

		public List<IUnit> Units
		{
			get { return units; }
		}

		public PlayerCombatent(Player player, List<PlayerUnit> units)
		{
			this.player = player;
			this.units = new List<IUnit>(units.Select(u => (IUnit)u));
		}

		public void BeginTurn()
		{
			//todo send all players a packet
		}

		public void StartBattle(Battle currentBattle, List<ICombatent> otherPlayers)
		{
		}

		public void BeginTurn(IUnit activeUnit)
		{
		}

		public void EndTurn()
		{
			//todo send all players a packet
		}
	}
}
