﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CommonUtilities;
using CommonUtilities.Unit;

namespace ProjectRhino.Battle
{
	class Battle
	{
		private List<ICombatent> combatents;
		private Random rng;
		private bool inProgress = false;
		private Semaphore unitTakingTurn = new Semaphore(1, 1);
		public Battle(List<ICombatent> combatents)
		{
			this.combatents = combatents;
			rng = new Random();
			inProgress = true;
		}

		public List<UnitTurn> FindNext()
		{
			List<UnitTurn> unitsReady = new List<UnitTurn>();
			foreach (var combatent in combatents)
			{
				foreach (var unit in combatent.Units)
				{
					if (unit.Alive && unit.Charge() >= 100)
					{
						unitsReady.Add(new UnitTurn(){unit = unit, combatent = combatent});
					}
				}
			}

			unitsReady.Shuffle();
			unitsReady.Sort((u, u2) => u.unit.CurrentCharge.CompareTo(u2.unit.CurrentCharge));
			return unitsReady;
		}

		public void PerformAttack(IUnit activeUnit, IUnit unitToTarget)
		{
			int damage = activeUnit.GetPhysicalDamage();
			float chanceToCrit = (activeUnit.GetDexterity() - unitToTarget.GetDexterity()) / (1.0f * unitToTarget.GetDexterity());
			if (chanceToCrit > rng.NextDouble())
			{
				Console.WriteLine("{0} hit a Critical Hit", activeUnit.Name);
				damage = (int)(damage * (1 + chanceToCrit));
			}
			Console.WriteLine("{0} attacks {1}({2}) with base damage {3}", activeUnit.Name, unitToTarget.Name, unitToTarget.CurrentHP, damage);
			unitToTarget.PhysicalAttack(damage);
		}

		public void EndTurn(IUnit unit)
		{
			unitTakingTurn.Release();
			int playersWithUnits = 0;
			Console.WriteLine("{0} turn ends.", unit.Name);
			foreach (var combatent in combatents)
			{
				if (!combatent.Units.Any(u => u.Alive))
				{
					playersWithUnits++;
				}
			}

			if (combatents.Count - playersWithUnits <= 1)
			{
				inProgress = false;
				Console.WriteLine("Battle ends.");
			}
		}

		public void BeginBattle()
		{
			foreach (var player in combatents)
			{
				List<ICombatent> players = new List<ICombatent>(combatents);
				players.Remove(player);
				player.StartBattle(this, players);
			}
			Console.WriteLine("Battle starts.");
			ThreadPool.QueueUserWorkItem(BattleLoop);
		}

		private void BattleLoop(object state)
		{
			while (inProgress)
			{
				List<UnitTurn> unitsReady = new List<UnitTurn>();
				do
				{
					unitsReady = FindNext();
				} while (unitsReady.Count == 0 && inProgress);

				while (unitsReady.Count > 0 && inProgress)
				{
					var unit = unitsReady[0].unit;
					var combatent = unitsReady[0].combatent;
					unitsReady.RemoveAt(0);
					Console.WriteLine("{0} begins turn.", unit.Name);
					if (unit.Alive && unit.CurrentCharge >= 100)
					{
						unit.SpendCharge(100);
						unitTakingTurn.WaitOne();
						combatent.BeginTurn(unit);
					}
				}
			}
		}
	}

	struct UnitTurn
	{
		public IUnit unit;
		public ICombatent combatent;
	}
}
