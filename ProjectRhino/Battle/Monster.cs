﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonUtilities.Attributes;

namespace ProjectRhino.Battle
{
	class Monster : IUnit
	{
		private static int count = 0;
		private int currentHP;
		private int currentMP;
		private int charge;
		private bool alive = true;
		private AttributeGroup attributes;
		private String name;

		public int MaxHP
		{
			get { return (int) attributes.HP.CalculateValue(); }
		}

		public int CurrentHP
		{
			get { return currentHP; }
		}

		public int MaxMP
		{
			get { return (int) attributes.MP.CalculateValue(); }
		}

		public int CurrentMP
		{
			get { return currentMP; }
		}

		public bool Alive
		{
			get { return alive; }
		}

		public int CurrentCharge
		{
			get { return charge; }
		}

		public string Name
		{
			get { return name; }
		}

		public Monster(string owner)
		{
			attributes = new AttributeGroup();
			name = owner + "'s Monster" + count++;
			currentHP = MaxHP;
			currentMP = MaxMP;
		}

		public int Charge()
		{
			return charge += (int) attributes.Agility.CalculateValue();
		}

		public void PhysicalAttack(int damage)
		{
			int defense = (int) attributes.PhysicalDefense.CalculateValue();
			if (damage <= defense)
				return;

			currentHP -= (damage - defense);
			if (currentHP <= 0)
			{
				currentHP = 0;
				alive = false;
			}
		}

		public int GetPhysicalDamage()
		{
			return (int) attributes.PhysicalAttack.CalculateValue();
		}

		public int GetDexterity()
		{
			return (int)attributes.Dexterity.CalculateValue();
		}

		public void SpendCharge(int i)
		{
			charge -= i;
		}
	}
}
