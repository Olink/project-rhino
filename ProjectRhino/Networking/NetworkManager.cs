using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CommonUtilities.Messages;
using Lidgren.Network;

using CommonUtilities;

namespace ProjectRhino.Networking
{
    public class NetworkManager : IMessenger
    {
        private readonly NetServer m_NetServer;

        private int m_Port;
        private bool m_ServerRunning = false;

        public Logger logger;

        public Version server_version = new Version(1, 0);

        readonly IServerMessageSerializer m_ServerSerializer;
        readonly IClientMessageSerializer m_ClientSerializer;

        public NetworkManager(int port, Version v, IServerMessageSerializer serverSerializer, IClientMessageSerializer clientSerializer, Logger logger)
        {
            m_Port = port;

            this.logger = logger;

            server_version = v;
            m_ServerSerializer = serverSerializer;
            m_ClientSerializer = clientSerializer;

            var config = new NetPeerConfiguration("Rhino") { Port = m_Port };

            m_NetServer = new NetServer(config);
        }

        public void Start()
        {
            m_ServerRunning = true;
            m_NetServer.Start();
            new Thread(Listen).Start();
        }

        public void Shutdown(string message)
        {
            m_ServerRunning = false;
            m_NetServer.MessageReceivedEvent.Set();
            m_NetServer.Shutdown(message);
        }

        private void Listen()
        {
            logger.Debug("Network Listener: Initialized.");
            NetIncomingMessage msg;
            while (m_ServerRunning)
            {
                m_NetServer.MessageReceivedEvent.WaitOne();
                while ((msg = m_NetServer.ReadMessage()) != null)
                {
                    logger.Verbose("Network Listener: Received message.");
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.ErrorMessage:
                            logger.Warn(string.Format("WARNING/ERROR: {0}", msg.ReadString()));
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                            
                            if (status == NetConnectionStatus.Disconnected)
                                OnReceiverRemoved(new NetReceiver(msg.SenderConnection));
                            else if (status == NetConnectionStatus.Connected)
                                OnReceiverAdded(new NetReceiver(msg.SenderConnection));
                            
                            break;
                        case NetIncomingMessageType.Data:
                            logger.Verbose(string.Format("Packet data from {0}", msg.SenderEndPoint));
                            try
                            {
                                var message = m_ClientSerializer.Deserialize(new MemoryStream(msg.Data));
                                OnMessageReceived(message, new NetReceiver(msg.SenderConnection));
                            }
                            catch (Exception e)
                            {
                               logger.Warn(e.Message);
                            }
                            break;
                    }
                    m_NetServer.Recycle(msg);
                }
            }
            logger.Debug("Network Listener: Shutting down.");
        }

        void OnMessageReceived(IMessage message, IReceiver sender)
        {
            if (MessageReceived != null)
                MessageReceived(this, new MessageReceivedEventArgs(message, sender, this));
        }

        void OnReceiverAdded(IReceiver receiver)
        {
            var netReceiver = receiver as NetReceiver;
            logger.Debug(string.Format("{0} has connected.", netReceiver.Connection));

            if (ReceiverAdded != null)
                ReceiverAdded(this, new ReceiverEventArgs(receiver));
        }

        void OnReceiverRemoved(IReceiver receiver)
        {
            var netReceiver = receiver as NetReceiver;
            logger.Debug(string.Format("{0} has disconnected.", netReceiver.Connection));

            if (ReceiverRemoved != null)
                ReceiverRemoved(this, new ReceiverEventArgs(receiver));
        }

        public IList<IReceiver> Receivers { get { return m_NetServer.Connections.Select(nc => (IReceiver)new NetReceiver(nc)).ToList(); } }
        public void SendMessage(IMessage msg, IReceiver receiver)
        {
            var netReceiver = receiver as NetReceiver;
            if (netReceiver == null)
                throw new NotSupportedException("Tried to send a message to an invalid receiver");

            using (var ms = new MemoryStream())
            {
                m_ServerSerializer.Serialize(msg, ms);
                var netMsg = m_NetServer.CreateMessage((int)ms.Length);
                netMsg.Write(ms.ToArray());
                m_NetServer.SendMessage(netMsg, netReceiver.Connection, NetDeliveryMethod.ReliableOrdered);
            }
        }

        public void SendAllMessage(IMessage msg)
        {
            using (var ms = new MemoryStream())
            {
                m_ServerSerializer.Serialize(msg, ms);
                var netMsg = m_NetServer.CreateMessage((int)ms.Length);
                netMsg.Write(ms.ToArray());
                m_NetServer.SendToAll(netMsg, NetDeliveryMethod.ReliableOrdered);
            }
        }

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        public event EventHandler<ReceiverEventArgs> ReceiverAdded;
        public event EventHandler<ReceiverEventArgs> ReceiverRemoved;
    }

    public class NetReceiver : IReceiver
    {
        readonly NetConnection m_Connection;

        public NetReceiver(NetConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException("connection");
            m_Connection = connection;
        }

        public NetConnection Connection
        {
            get { return m_Connection; }
        }

        public override int GetHashCode()
        {
            return m_Connection.RemoteEndPoint.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if(obj is NetReceiver)
            {
                var rec = obj as NetReceiver;
                return rec.Connection.RemoteEndPoint == m_Connection.RemoteEndPoint;
            }
            return false;
        }
    }
}
