﻿namespace ProjectRhino.UserAccounts
{
	interface IUserStorage
	{
		IUser GetUser(string name);
		bool UserExists(string name);
		bool VerifyPassword(string name, string password);
		void AddUser(IUser u);
		void SaveStorage();
	}
}
