﻿using System;
using System.IO;
using CommonUtilities.Item;
using CommonUtilities.Unit;
using Newtonsoft.Json;
using ProjectRhino.Crypto;

using UserDatabase = System.Collections.Generic.Dictionary<string, ProjectRhino.UserAccounts.JsonUserAccounts.JsonUser>;

namespace ProjectRhino.UserAccounts.JsonUserAccounts
{
	class JsonUserStorage : IUserStorage
	{
		UserDatabase userAccounts = new UserDatabase();
		private SaltedMD5 md5 = new SaltedMD5();
		private string savePath = "";

		public JsonUserStorage(string path)
		{
			savePath = path;

			if (!File.Exists(path))
				return;

			using(var reader = new StreamReader(path))
			{
				userAccounts = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDatabase>(reader.ReadToEnd());
			}
		}

		public IUser GetUser(string name)
		{
			if (userAccounts.ContainsKey(name))
			{
				var acc = userAccounts[name];
				if (acc.Roster == null)
					acc.Roster = Roster.DefaultRoster();
				if (acc.Inventory == null)
					acc.Inventory = Inventory.DefaultInventory();
				return userAccounts[name];
			}


			throw new InvalidOperationException("User does not exist.");
		}

		public bool UserExists(string name)
		{
			return userAccounts.ContainsKey(name);
		}

		public bool VerifyPassword(string name, string password)
		{
			if (!userAccounts.ContainsKey(name))
				throw new InvalidOperationException("User does not exist.");

			return (userAccounts[name].Password.Equals(md5.SaltString(password)));
		}

		public void AddUser(IUser u)
		{
			if (userAccounts.ContainsKey(u.Username))
				throw new InvalidOperationException("User already exists.");

			var jsonUser = u as JsonUser;
			jsonUser.Password = md5.SaltString(jsonUser.Password);
			userAccounts.Add(u.Username, (JsonUser)u);
			SaveStorage();
		}

		public void SaveStorage()
		{
			using (var writer = new StreamWriter(savePath))
			{
				String json = Newtonsoft.Json.JsonConvert.SerializeObject(userAccounts, Formatting.Indented);
				writer.Write(json);
			}
		}
	}
}
