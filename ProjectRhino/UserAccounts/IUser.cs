﻿using CommonUtilities.Item;
using CommonUtilities.Unit;

namespace ProjectRhino.UserAccounts
{
	public abstract class IUser
	{
		public string Username { get; set; }
		public Roster Roster { get; set; }
		public Inventory Inventory { get; set; }
	}
}
