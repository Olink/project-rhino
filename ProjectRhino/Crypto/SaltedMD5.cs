﻿using CommonUtilities.Crypto;

namespace ProjectRhino.Crypto
{
	class SaltedMD5 : BasicMD5
	{
		protected override string Salt
		{
			get { return "2f4435ac37e66527e2f15d83913ea7aa"; }
		}
	}
}
