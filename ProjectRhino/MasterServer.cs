using System;
using System.Collections.Generic;
using System.Linq;
using CommonUtilities;
using CommonUtilities.Item;
using CommonUtilities.Messages;
using CommonUtilities.Messages.Client;
using CommonUtilities.Messages.Server;
using CommonUtilities.Messaging;
using CommonUtilities.Players;
using CommonUtilities.Unit;

using Ninject;
using Ninject.Extensions.ChildKernel;
using Ninject.Parameters;
using ProjectRhino.Battle;
using ProjectRhino.Networking;
using ProjectRhino.UserAccounts;
using ProjectRhino.UserAccounts.JsonUserAccounts;
using CommonUtilities.Jobs;

namespace ProjectRhino
{
	class MasterServer
	{
		private IMessenger m_Server;
		readonly Version m_Version = new Version(1, 0);
		IKernel m_ServerContainer;
		readonly Logger m_Logger;

		private IUserStorage m_UserStorage;
		public PlayerManager<Player> m_PlayerManager;

		private ChatRoom m_GlobalChat;
		private ChatManager<ChatRoom> m_ChatManager;
		private IItemLoader m_ItemLoader;
		private IJobManager m_JobManager;
		public MasterServer(IKernel container, Logger logger, int port)
		{
			m_Logger = logger;

			m_Server = container.Get<NetworkManager>(new ConstructorArgument("port", port), new ConstructorArgument("v", m_Version));
			
			m_Server.MessageReceived += MessageReceived;
			m_Server.ReceiverRemoved += ReceiverRemoved;
			m_Server.ReceiverAdded += ReceiverAdded;

			m_ServerContainer = new ChildKernel(container);
			m_ServerContainer.Bind<IMessenger>().ToConstant(m_Server);

			m_UserStorage = new JsonUserStorage("useraccounts.json");
			m_PlayerManager = new PlayerManager<Player>();
			m_GlobalChat = new ChatRoom("Global");
			m_ChatManager = new ChatManager<ChatRoom>();
			m_ChatManager.AddChatroom(m_GlobalChat);
			m_ItemLoader = new JsonItemLoader("data/items");
			m_JobManager = new JsonJobManager("data/jobs");
			new JobFactory(m_JobManager);

			Console.Title = String.Format("Nyx Studios Rhino Server: {0}", m_Version);
			((NetworkManager)m_Server).Start();
		}

		public void Shutdown()
		{
			m_UserStorage.SaveStorage();
			m_Server.SendAllMessage(new CommonUtilities.Messages.Server.Disconnect() { Reason = "Server is shutting down..." });
			((NetworkManager)m_Server).Shutdown("Server has been shut down.");
		}

		//This is only an example.  We still need to define the design that specifies where these handlers should be.
		private void MessageReceived(object sender, MessageReceivedEventArgs args)
		{
			Player ply = m_PlayerManager.FindPlayer(args.Sender);
			if (ply == null)
				return;

			m_Logger.Verbose(string.Format("{0} packet received from {1}", args.Message.GetType(), args.Sender));

			if (args.Message is VersionRequest)
			{
				if (ply.State != ConnectionState.None)
					return;

				ply.State = ConnectionState.Connected;
				args.Messenger.SendMessage(new VersionResponse(true, m_Version.ToString()), args.Sender);
			}
			else if(args.Message is RegisterRequest)
			{
				if (ply.State != ConnectionState.Connected)
					return;

				var msg = args.Message as RegisterRequest;
				UserRegisterAttempt(msg.Name, msg.Password, ply);
			}
			else if(args.Message is LoginRequest)
			{
				if (ply.State != ConnectionState.Connected)
					return;

				var msg = args.Message as LoginRequest;
				UserLoginAttempt(msg.Name, msg.Password, ply);
			}
			else if(args.Message is CommonUtilities.Messages.Client.Chat)
			{
				if (ply.State != ConnectionState.LoggedIn)
					return;

				var msg = args.Message as CommonUtilities.Messages.Client.Chat;
				ChatRoom room = m_ChatManager.GetChatroom(msg.UUID);
				if(room != null)
					room.Chat(String.Format("{0}: {1}", ply.Name, msg.Message));

				if (msg.Message == "battle")
				{
					List<ICombatent> players = new List<ICombatent>();
					players.Add(new AICombatent());
					players.Add(new AICombatent());
					players.Add(new AICombatent());
					players.Add(new AICombatent());
					Battle.Battle b = new Battle.Battle(players);
					b.BeginBattle();
				}
			}
			else if(args.Message is RosterRequest)
			{
				if(ply.State == ConnectionState.LoggedIn)
				{
					ply.SendMessage(new RosterResponse(ply.User.Roster));
				}
			}
			else if (args.Message is EquipUnit)
			{
				if (ply.State != ConnectionState.LoggedIn)
					return;

				var msg = args.Message as EquipUnit;
				if(msg.ItemIndex < 0)
					ply.RemoveItem(msg.UnitIndex, msg.Slot);
				else
					ply.EquipItem(msg.ItemIndex, msg.UnitIndex, msg.Slot);
			}
			else if(args.Message is RequestBattle)
			{			
			}
			else if(args.Message is CommonUtilities.Messages.Client.BattleAction)
			{
			}
		}

		private void ReceiverRemoved(object sender, ReceiverEventArgs args)
		{
			var ply = m_PlayerManager.FindPlayer(args.Receiver);
			m_PlayerManager.RemovePlayer(ply);

			if(ply != null && ply.State == ConnectionState.LoggedIn)
			{
				m_ChatManager.PlayerLeft(ply);

				foreach (var p in m_PlayerManager.GetPlayers().Where(x => x.State == ConnectionState.LoggedIn))
				{
					p.SendMessage(new ServerLeave(ply.UUID));
				}
			}
		}

		private void ReceiverAdded(object sender, ReceiverEventArgs args)
		{
			var ply = new Player(args.Receiver, m_Server) {State = ConnectionState.None, UUID = m_PlayerManager.FindUniqueUUID()};
			m_PlayerManager.AddPlayer(ply);
		}

		private void UserLoginAttempt(string name, string password, Player ply)
		{
			if(m_UserStorage.UserExists(name))
			{
				if(m_UserStorage.VerifyPassword(name, password))
				{
					ply.User = m_UserStorage.GetUser(name);
					ply.Name = name;

					foreach(var p in m_PlayerManager.GetPlayers().Where(x=>x.State == ConnectionState.LoggedIn))
					{
						p.SendMessage(new ServerJoin(ply.UUID, ply.Name));
					}

					ply.State = ConnectionState.LoggedIn;
					ply.SendMessage(new LoginResponse(true, ""));
					ply.SendMessage(new RosterResponse(ply.User.Roster));
					ply.SendMessage(new InventoryResponse(ply.User.Inventory));
					ply.SendMessage(new PlayerJoinChatroom(m_GlobalChat.CreateDummy()));
					m_GlobalChat.Join(ply, "");
				}
				else
				{
					ply.SendMessage(new LoginResponse(false, "Invalid password for " + name));
				}
			}
			else
			{
				ply.SendMessage(new LoginResponse(false, "User does not exist for " + name));
			}
		}

		private void UserRegisterAttempt(string name, string password, Player ply)
		{
			if (!m_UserStorage.UserExists(name))
			{
				var user = new JsonUser {Password = password, Username = name, Roster = Roster.DefaultRoster(), Inventory = Inventory.DefaultInventory()};
				m_UserStorage.AddUser(user);
				ply.SendMessage(new RegisterResponse(true, ""));
			}
			else
			{
				ply.SendMessage(new RegisterResponse(false, "User already exists for " + name));
			}
		}
	}
}
