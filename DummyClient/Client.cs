using System;
using System.Linq;
using CommonUtilities.Crypto;
using CommonUtilities.Item;
using CommonUtilities.Messages;
using CommonUtilities.Messaging;
using CommonUtilities.Players;
using CommonUtilities.Unit;
using DummyClient.Networking;
using Ninject;
using Ninject.Parameters;
using Ninject.Extensions.ChildKernel;
using CommonUtilities;
using CommonUtilities.Messages.Server;
using CommonUtilities.Messages.Client;
using System.Threading;

namespace DummyClient
{
	class Client
	{
		readonly IKernel m_Container;

		private NetworkManager m_Client;

		IKernel m_ClientContainer;
		
		readonly Logger m_Logger;

		readonly Version m_Version = new Version(1, 0);

		private PlayerManager<DummyPlayer> m_PlayerManager;
		private ChatManager<DummyChatroom> m_ChatManager;
		private DummyChatroom m_DefaultChatroom = null;
		private Roster m_Roster = new Roster();
		private Inventory m_Inventory = new Inventory();
		private string m_CurrentUnit = "";

		public Client(IKernel container, Logger logger, string host, int port)
		{
			m_Container = container;
			m_Logger = logger;

			m_Client = container.Get<NetworkManager>(new ConstructorArgument("host", host), new ConstructorArgument("port", port));
			m_Client.MessageReceived += MessageReceived;

			m_ClientContainer = new ChildKernel(container);
			m_ClientContainer.Bind<IMessenger>().ToConstant(m_Client);

			m_PlayerManager = new PlayerManager<DummyPlayer>();
			m_ChatManager = new ChatManager<DummyChatroom>();

			Console.Title = String.Format("Nyx Studios Rhino Dummy Client: {0}", m_Version);
		}

		public void Shutdown(string msg)
		{
			m_Client.Shutdown(msg);
		}

		public void Start()
		{
			m_Client.Start();
			new Thread(ListenInput).Start();
		}

		public void ListenInput()
		{
			string input = string.Empty;
			do
			{
				input = Console.ReadLine();
				var parameters = CommonUtilities.Utilities.BreakIntoParameters(input);
				if (parameters.Length > 0)
				{
					switch (parameters[0])
					{
						case "version":
						{
							m_Client.SendMessage(new VersionRequest(m_Version.ToString()), null);
						}
							break;
						case "login":
						{
							if (parameters.Length < 2)
							{
								break;
							}

							string name = parameters[1];
							string password = "";

							if (parameters.Length > 2)
							{
								password = parameters[2];
							}

							password = new BasicMD5().ComputeHash(password);
							m_Client.SendMessage(new LoginRequest(name, password), null);
						}
							break;
						case "register":
						{
							if (parameters.Length < 2)
							{
								break;
							}

							string name = parameters[1];
							string password = "";

							if (parameters.Length > 2)
							{
								password = parameters[2];
							}

							password = new BasicMD5().ComputeHash(password);
							m_Client.SendMessage(new RegisterRequest(name, password), null);
						}
							break;
						case "say":
						{
							if (parameters.Length < 2)
							{
								break;
							}

							string msg = String.Join(" ", parameters.Skip(1));

							m_Client.SendMessage(new CommonUtilities.Messages.Client.Chat(msg, m_DefaultChatroom.UUID), null);
						}
							break;
						case "roster":
						{
							m_Client.SendMessage(new RosterRequest(), null);
						}
							break;
						case "display":
						{
							if (parameters.Length != 2)
								break;

							switch (parameters[1])
							{
								case "roster":
									Utilities.ParseAndOutputChat(m_Roster.ToString());
									break;
								case "inventory":
									Utilities.ParseAndOutputChat(m_Inventory.ToString());
									break;
							}
						}
							break;
						case "equip":
							if (parameters.Length < 4)
								break;

							int itemslot = int.Parse(parameters[1]);
							int unitslot = int.Parse(parameters[2]);
							EquipmentSlot equipSlot;
							Enum.TryParse(parameters[3], out equipSlot);
							m_Client.SendMessage(new EquipUnit(itemslot, unitslot, equipSlot), null);
							break;
						case "dequip":
							if (parameters.Length < 3)
								break;

							int unitslot2 = int.Parse(parameters[1]);
							EquipmentSlot equipSlot2;
							Enum.TryParse(parameters[2], out equipSlot2);
							m_Client.SendMessage(new EquipUnit(-1, unitslot2, equipSlot2), null);
							break;
						case "battle":
							m_Client.SendMessage(new RequestBattle(parameters[1]), null);
							break;
						case "wait":
							m_Client.SendMessage(new BattleAction(m_CurrentUnit, BattleActionEnum.Wait, ""), null);
							break;
						case  "defend":
							m_Client.SendMessage(new BattleAction(m_CurrentUnit, BattleActionEnum.Defend, ""), null);
							break;
						case "attack":
							m_Client.SendMessage(new BattleAction(m_CurrentUnit, BattleActionEnum.Attack, parameters[1]), null);
							break;
						default:
							m_Logger.Warn("That is not a valid command.");
							break;
					}
				}
			} while (input != string.Empty);

			Shutdown("Client quit.");
		}

		//This is only an example.  We still need to define the design that specifies where these handlers should be.
		private void MessageReceived(object sender, MessageReceivedEventArgs args)
		{
			m_Logger.Verbose(string.Format("{0} packet received from {1}", args.Message.GetType(), args.Sender));

			if (args.Message is VersionResponse)
			{
				var msg = args.Message as VersionResponse;
				HandleVersionResponse(msg.Allowed, msg.Version);
			}
			else if(args.Message is LoginResponse)
			{
				var msg = args.Message as LoginResponse;
				HandleLoginResponse(msg.Success, msg.Error);
			}
			else if(args.Message is CommonUtilities.Messages.Server.Chat)
			{
				var msg = args.Message as CommonUtilities.Messages.Server.Chat;
				HandleChat(msg.Message, msg.UUID);
			}
			else if(args.Message is Disconnect)
			{
				var msg = args.Message as Disconnect;
				Shutdown(msg.Reason);
			}
			else if(args.Message is RegisterResponse)
			{
				var msg = args.Message as RegisterResponse;
				HandleRegisterResponse(msg.Success, msg.Error);
			}
			else if(args.Message is RosterResponse)
			{
				var msg = args.Message as RosterResponse;
				HandleRosterResponse(msg.Roster);
			}
			else if (args.Message is PlayerJoinChatroom)
			{
				var msg = args.Message as PlayerJoinChatroom;
				if (m_DefaultChatroom == null)
					m_DefaultChatroom = msg.Room;
				m_ChatManager.AddChatroom(msg.Room);
			}
			else if (args.Message is ChatroomUserJoin)
			{
				var msg = args.Message as ChatroomUserJoin;
				var room = (DummyChatroom)m_ChatManager.GetChatroom(msg.UUID);
				if (room != null)
				{
					var ply = m_PlayerManager.FindPlayer(msg.PlayerUUID);
					if(ply != null)
						room.Join(ply, "");
				}
			}
			else if (args.Message is ChatroomUserLeave)
			{
				var msg = args.Message as ChatroomUserLeave;
				var room = (DummyChatroom)m_ChatManager.GetChatroom(msg.UUID);
				if (room != null)
				{
					var ply = m_PlayerManager.FindPlayer(msg.PlayerUUID);
					if (ply != null)
						room.LeaveRoom(ply);
				}
			}
			else if (args.Message is ServerJoin)
			{
				var msg = args.Message as ServerJoin;

				m_PlayerManager.AddPlayer(new DummyPlayer(msg.UUID, msg.Name));
			}
			else if(args.Message is ServerLeave)
			{
				var msg = args.Message as ServerLeave;

				var ply = m_PlayerManager.FindPlayer(msg.UUID);
				if(ply != null)
				{
					m_ChatManager.PlayerLeft(ply);
					m_PlayerManager.RemovePlayer(ply);
				}
			}
			else if(args.Message is InventoryResponse)
			{
				var msg = args.Message as InventoryResponse;

				m_Inventory = msg.Inventory;
				Utilities.ParseAndOutputChat(m_Inventory.ToString());
			}
			else if (args.Message is InventorySlot)
			{
				var msg = args.Message as InventorySlot;

				if (msg.Item is EmptyEquipment && msg.Index < m_Inventory.Contents.Count)
					m_Inventory.Contents.RemoveAt(msg.Index);
				else if (msg.Index >= m_Inventory.Contents.Count)
					m_Inventory.AddItem(msg.Item);
				else
					m_Inventory.Contents[msg.Index] = msg.Item;
			}
			else if (args.Message is UnitUpdate)
			{
				var msg = args.Message as UnitUpdate;

				m_Roster.Units[msg.Index] = msg.Unit;
			}
			else if(args.Message is BattleStartTurn)
			{
				var msg = args.Message as BattleStartTurn;

				m_CurrentUnit = msg.UnitUUID;
			}
		}

		private void HandleVersionResponse(bool allowed, String version)
		{
			Version v;
			if(allowed)
			{
				m_Logger.Notify("Client version is allowed by server");
			}
			else
			{
				m_Logger.Notify("Client version is not allowed by server");
			}
			if (Version.TryParse(version, out v))
			{
				if (v == m_Version)
				{
					m_Logger.Notify("Client matches server version");
				}
				else
				{
					m_Logger.Notify("Client does not match server version");
				}
			}
			else
			{
				m_Logger.Notify("Could not parse version string from server.");
			}
		}

		private void HandleLoginResponse(bool success, string error)
		{
			if (success)
				m_Logger.Notify("Successfully logged in.");
			else
			{
				m_Logger.Notify(string.Format("Failed to log in: {0}", error));
			}
		}

		private void HandleRegisterResponse(bool success, string error)
		{
			if (success)
				m_Logger.Notify("Successfully registered account.");
			else
			{
				m_Logger.Notify(string.Format("Failed to register: {0}", error));
			}
		}

		private void HandleChat(string message, string uuid)
		{
			DummyChatroom channel = (DummyChatroom)m_ChatManager.GetChatroom(uuid);
			if(channel != null)
				channel.Chat(message);
			//todo handle this better because on a gui youd have tabs and want to update each room etc.
		}

		private void HandleRosterResponse(Roster r)
		{
			m_Roster = r;
			Utilities.ParseAndOutputChat(r.ToString());
		}
	}
}
