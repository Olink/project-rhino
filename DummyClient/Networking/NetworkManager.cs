using System;
using System.Collections.Generic;
using System.Threading;
using Lidgren.Network;

using CommonUtilities;
using CommonUtilities.Messages;
using System.IO;

namespace DummyClient.Networking
{
	internal class NetworkManager : IMessenger
	{
		private bool m_ClientRunning = false;
		private NetClient m_Client;

		public Version clientVersion = new Version(1, 0);
		readonly IServerMessageSerializer m_ServerSerializer;
		readonly IClientMessageSerializer m_ClientSerializer;

		readonly string m_Host;
		readonly int m_Port;

		readonly Logger m_Logger;

		public NetworkManager(string host, int port, IServerMessageSerializer serverSerializer, IClientMessageSerializer clientSerializer, Logger logger)
		{
			m_ServerSerializer = serverSerializer;
			m_ClientSerializer = clientSerializer;

			m_Logger = logger;

			m_Host = host;
			m_Port = port;

			var config = new NetPeerConfiguration("Rhino");
			config.EnableMessageType(NetIncomingMessageType.Data);
			m_Client = new NetClient(config);
		}

		public void Start()
		{
			DateTime lastAttempt = DateTime.Now;
			m_Logger.Debug("Connecting to server");
			m_ClientRunning = true;
			m_Client.Start();
			m_Client.Connect(m_Host, m_Port);
			
			new Thread(Listen).Start();

			while (m_Client.ConnectionStatus != NetConnectionStatus.Connected)
			{
			}

			m_Logger.Debug("Connected to server");
		}

		private void Listen()
		{
			NetIncomingMessage msg;
			while (m_ClientRunning)
			{
				m_Client.MessageReceivedEvent.WaitOne();
				while ((msg = m_Client.ReadMessage()) != null)
				{
					switch (msg.MessageType)
					{
						case NetIncomingMessageType.VerboseDebugMessage:
						case NetIncomingMessageType.DebugMessage:
						case NetIncomingMessageType.WarningMessage:
							Console.WriteLine(msg.ReadString());
							break;
						case NetIncomingMessageType.ErrorMessage:
							m_Logger.Warn(string.Format("ERROR: {0}", msg.ReadString()));
							break;
						case NetIncomingMessageType.Data:
							m_Logger.Verbose(string.Format("Incoming Message from {0}", msg.SenderEndPoint));
							try
							{
								var message = m_ServerSerializer.Deserialize(new MemoryStream(msg.Data));
								OnMessageReceived(message, null);
							}
							catch (Exception)
							{
								m_Logger.Warn("What the fuck packet was this");
							}
							break;
					}
					m_Client.Recycle(msg);
				}
			}
		}

		public void OnMessageReceived(IMessage message, IReceiver receiver)
		{
			if (MessageReceived != null)
			{
				MessageReceived(this, new MessageReceivedEventArgs(message, receiver, this));
			}
		}

		void OnReceiverAdded(IReceiver receiver)
		{
			if (ReceiverAdded != null)
				ReceiverAdded(this, new ReceiverEventArgs(receiver));
		}

		void OnReceiverRemoved(IReceiver receiver)
		{
			if (ReceiverRemoved != null)
				ReceiverRemoved(this, new ReceiverEventArgs(receiver));
		}

		public void SendMessage(IMessage msg, IReceiver receiver)
		{
			using (var ms = new MemoryStream())
			{
				m_ClientSerializer.Serialize(msg, ms);
				var netMsg = m_Client.CreateMessage((int)ms.Length);
				netMsg.Write(ms.ToArray());
				m_Client.SendMessage(netMsg, NetDeliveryMethod.ReliableOrdered);
			}
		}

		public void SendAllMessage(IMessage msg) 
		{
			SendMessage(msg, null);
		}

		public void Shutdown(string message)
		{
			m_ClientRunning = false;
			m_Client.MessageReceivedEvent.Set();
			m_Client.Shutdown(message);
		}

		public event EventHandler<MessageReceivedEventArgs> MessageReceived;
		public event EventHandler<ReceiverEventArgs> ReceiverAdded;
		public event EventHandler<ReceiverEventArgs> ReceiverRemoved;

		public IList<IReceiver> Receivers { get { return null; } }
	}
}
