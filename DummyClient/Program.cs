﻿using System;
using System.Threading;
using CommonUtilities;
using CommonUtilities.Messages;
using Ninject;
using Ninject.Parameters;

namespace DummyClient
{
	class Program
	{
		public static int ServerPort = 27010;
		public static string ServerHost = "127.0.0.1";
		public static readonly IKernel Container = new StandardKernel();
		Logger m_Logger;

		public Program()
		{
			Thread.Sleep(3000);

			m_Logger = new Logger("log.txt");
			m_Logger.SetLogLevel(31);

			Container.Bind<IServerMessageSerializer>().To(typeof(ServerMessageSerializer));
			Container.Bind<IClientMessageSerializer>().To(typeof(ClientMessageSerializer));
			Container.Bind<Logger>().ToConstant(m_Logger);
			Container.Bind<Client>().To(typeof(Client));

			var client = Container.Get<Client>(new ConstructorArgument("host", ServerHost), new ConstructorArgument("port", ServerPort));
			client.Start();
		}

		static void Main(string[] args)
		{
			if (args.Length > 0)
			{
				int port = 0;
				if (Int32.TryParse(args[0], out port))
					ServerPort = port;
			}
			if (args.Length > 1)
			{
				string host = "";
				ServerHost = host;
			}
			new Program();
		}
	}
}
