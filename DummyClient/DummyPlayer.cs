using CommonUtilities.Messages;
using CommonUtilities.Players;

namespace DummyClient
{
	public class DummyPlayer : IPlayer
	{
		public DummyPlayer(string playerUuid, string name)
		{
			UUID = playerUuid;
			Name = name;
		}

		public string UUID { get; set; }
		public string Name { get; private set; }
		public IReceiver Connection { get; private set; }
		public void SendMessage(IMessage msg)
		{
		}
	}
}