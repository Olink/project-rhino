﻿using CommonUtilities.Players;

namespace CommonUtilities.Messaging
{
	class PrivateChannel : ChatRoom
	{
		public PrivateChannel(string name, IPlayer owner, string password) : base(name, owner)
		{
			Locked = true;
			Password = password;
		}
	}
}
