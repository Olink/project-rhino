﻿using System;
using CommonUtilities.Messages.Server;
using CommonUtilities.Players;

namespace CommonUtilities.Messaging
{
	class PrivateMessage : ChatRoom
	{
		public PrivateMessage(String name, IPlayer owner, IPlayer partner)
			: base(name, owner)
		{
			Locked = true;
			AllowedUUID.Add(partner.UUID);
			owner.SendMessage(new PlayerJoinChatroom(CreateDummy()));
			partner.SendMessage(new PlayerJoinChatroom(CreateDummy()));
		}
	}
}
