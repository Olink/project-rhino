﻿using System.Collections.Generic;
using CommonUtilities.Players;

namespace CommonUtilities.Messaging
{
	public class ChatManager<T> where T : ChatRoom
	{
		private Dictionary<string, T> m_ListOfRooms = new Dictionary<string, T>();

		public void AddChatroom(T r)
		{
			m_ListOfRooms.Add(r.UUID, r);
		}

		public ChatRoom GetChatroom(string uuid)
		{
			if (m_ListOfRooms.ContainsKey(uuid))
			{
				return m_ListOfRooms[uuid];
			}

			return null;
		}

		public void DeleteChatroom(string uuid)
		{
			if (m_ListOfRooms.ContainsKey(uuid))
			{
				m_ListOfRooms[uuid].Close();
				m_ListOfRooms.Remove(uuid);
			}
		}

		public void PlayerLeft(IPlayer ply)
		{
			foreach(var room in m_ListOfRooms.Values)
			{
				room.LeaveRoom(ply);
			}
		}
	}
}
