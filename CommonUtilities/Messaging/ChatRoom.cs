﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommonUtilities.Concepts;
using CommonUtilities.Messages;
using CommonUtilities.Messages.Server;
using CommonUtilities.Players;

namespace CommonUtilities.Messaging
{
	public class ChatRoom : ISerializable
	{
		static List<string> UUIDS = new List<string>(); 
		
		protected List<IPlayer> Participants { get; set; }
		protected List<string> AllowedUUID { get; set; }
		protected bool Locked { get; set; }
		protected string Password { get; set; }

		private IPlayer Owner { get; set; }
		
		public string UUID { get; set; }
		public string Name { get; set; }

		public ChatRoom() { }

		public ChatRoom(String name)
		{
			Name = name;
			Participants = new List<IPlayer>();
			AllowedUUID = new List<string>();
			Locked = false;
			Password = "";

			string uuid = System.Guid.NewGuid().ToString();
			while (UUIDS.Any(u => u == uuid))
				uuid = System.Guid.NewGuid().ToString();
			UUID = uuid;
		}

		public ChatRoom(String name, IPlayer owner)
		{
			Name = name;
			Owner = owner;
			Participants = new List<IPlayer>() { owner };
			AllowedUUID = new List<string>();
			Locked = false;
			Password = "";

			string uuid = System.Guid.NewGuid().ToString();
			while(UUIDS.Any(u=>u == uuid))
				uuid = System.Guid.NewGuid().ToString();
			UUID = uuid;
		}

		public virtual bool Join(IPlayer ply, string password = "")
		{
			if(Locked && (Password == "" || password != Password))
			{
				return false;
			}

			if (AllowedUUID.Count > 0 && !AllowedUUID.Contains(ply.UUID))
			{
				return false;
			}

			if(Participants.Contains(ply))
			{
				ply.SendMessage(new PlayerJoinChatroom(CreateDummy()));
				return true;
			}

			Participants.Add(ply);
			SendMessage(new ChatroomUserJoin(UUID, ply.UUID));
			return true;
		}

		public virtual bool KickPlayer(IPlayer rekt)
		{
			if (Participants.Contains(rekt))
			{
				SendMessage(new ChatroomUserLeave(UUID, rekt.UUID));
				return Participants.Remove(rekt);
			}
			return false;
		}

		public virtual void LeaveRoom(IPlayer ply)
		{
			if(Owner != null && Owner == ply)
			{
				Close();
			}
			else if(Participants.Contains(ply))
			{
				SendMessage(new ChatroomUserLeave(UUID, ply.UUID));
				Participants.Remove(ply);
			}
		}

		public virtual void Chat(String message)
		{
			SendMessage(new Chat(message, UUID));
		}

		public virtual void Close()
		{
			Locked = true;
			Password = "";
			foreach(var ply in Participants)
				SendMessage(new ChatroomUserLeave(UUID, ply.UUID));
			Participants.Clear();
		}

		public virtual void SendMessage(IMessage message)
		{
			foreach(var p in Participants)
			{
				p.SendMessage(message);
			}
		}

		public virtual void Write(Stream s)
		{
			s.WriteString(UUID);
			s.WriteString(Name);
			s.WriteBoolean(Locked);
		}

		public virtual void Read(Stream s)
		{
			UUID = s.ReadString();
			Name = s.ReadString();
			Locked = s.ReadBoolean();
		}

		public DummyChatroom CreateDummy()
		{
			return new DummyChatroom() {Locked = Locked, Name = Name, UUID = UUID, UUIDs = new List<string>(Participants.Select(s => s.UUID))};
		}
	}
}
