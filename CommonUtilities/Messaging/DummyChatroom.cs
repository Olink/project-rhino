﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonUtilities.Messages;
using CommonUtilities.Players;

namespace CommonUtilities.Messaging
{
	public class DummyChatroom : ChatRoom
	{
		public List<string> UUIDs { get; set; } 

		public DummyChatroom(){}
		public DummyChatroom(string name) : base(name)
		{
		}

		public DummyChatroom(string name, IPlayer owner) : base(name, owner)
		{
		}

		public override bool Join(IPlayer ply, string password = "")
		{
			UUIDs.Add(ply.UUID);
			Utilities.ParseAndOutputChat(String.Format("{0} has joined {1}", ply.Name, Name));
			return true;
		}

		public override bool KickPlayer(IPlayer rekt)
		{
			return true;
		}

		public override void LeaveRoom(IPlayer ply)
		{
			UUIDs.Remove(ply.UUID);
			Utilities.ParseAndOutputChat(String.Format("{0} has left {1}", ply.Name, Name));
		}

		public override void Chat(String message)
		{
			Utilities.ParseAndOutputChat(String.Format("{0}: {1}", Name, message));
		}

		public override void Close()
		{
		}

		public override void SendMessage(IMessage message)
		{
		}

		public override void Write(Stream s)
		{
			s.WriteString(UUID);
			s.WriteString(Name);
			s.WriteBoolean(Locked);
			s.WriteInt32(UUIDs.Count);
			foreach(var u in UUIDs)
				s.WriteString(u);
		}

		public override void Read(Stream s)
		{
			UUID = s.ReadString();
			Name = s.ReadString();
			Locked = s.ReadBoolean();
			int count = s.ReadInt32();
			UUIDs = new List<string>(count);
			for(int i = 0; i < count; i++)
				UUIDs.Add(s.ReadString());
		}
	}
}
