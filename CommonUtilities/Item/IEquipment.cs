﻿namespace CommonUtilities.Item
{
	public abstract class IEquipment : IItem
	{
	}

	public enum EquipmentSlot
	{
		LeftHand,
        RightHand,
		Head,
		Armor,
		Glove,
		Boots,
		Accessory
	}
}
