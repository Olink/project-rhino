﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	public class Weapon : IEquipment
	{
		private string name;
		private string description;
		private int price;
		private int sellingPrice;
        private WeaponSize size;
		private string uuid;

		public override string Name { get { return name; } set { name = value; } }
		public override string Description { get { return description; } set { description = value; } }
		public override int Price { get { return price; } set { price = value; } }
		public override int SellingPrice { get { return sellingPrice; } set { sellingPrice = value; } }
		public override string UUID { get { return uuid; } set { uuid = value; } }
        public WeaponSize Size { get { return size; } }


		public Weapon(string n, string d, int p, int sp, WeaponSize si)
		{
			name = n;
			description = d;
			price = p;
			sellingPrice = sp;
		}

		public override IItem Clone()
		{
			var weapon = new Weapon(name, description, price, sellingPrice, size);
			return weapon;
		}

		public override void Write(Stream s)
		{
			s.WriteString("weapon");
			base.Write(s);
            s.WriteString(size.ToString());
		}

        public override void Read(Stream s)
        {
            base.Read(s);
            WeaponSize si;
            Enum.TryParse(s.ReadString(), out si);
            size = si;
        }

		public static Weapon FromStream(Stream s)
		{
			var name = s.ReadString();
			var desc = s.ReadString();
			var price = s.ReadInt32();
			var selling = s.ReadInt32();
			var uuid = s.ReadString();
            WeaponSize size;
            Enum.TryParse(s.ReadString(), out size);

			return new Weapon(name, desc, price, selling, size){UUID = uuid};
		}

		public override void ToJson(JsonWriter writer)
		{
			writer.WriteStartObject();
			writer.WritePropertyName("ItemType");
			writer.WriteValue("weapon");
			base.ToJson(writer);
            writer.WritePropertyName("WeaponSize");
            writer.WriteValue(Size.ToString());
			writer.WriteEndObject();
		}

		public static Weapon FromJson(JsonReader reader)
		{
			reader.Read();
			var name = reader.ReadAsString();
			reader.Read();
			var desc = reader.ReadAsString();
			reader.Read();
			var price = reader.ReadAsInt32();
			reader.Read();
			var selling = reader.ReadAsInt32();
			reader.Read();
            WeaponSize size;
            Enum.TryParse(reader.ReadAsString(), out size);
            reader.Read();
			return new Weapon(name, desc, !price.HasValue ? 0 : price.Value, !selling.HasValue ? 0 : selling.Value, size);
		}

		public override string ToString()
		{
			return String.Format("Weapon: {0} - {1}.  Sells for {2}, bought for {3}.", name, description, sellingPrice, price);
		}
	}

    public enum WeaponSize
    {
        OneHanded,
        TwoHanded,
    }
}
