﻿using System.IO;
using CommonUtilities.Concepts;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	[JsonConverter(typeof(IItemJsonConverter))]
	public abstract class IItem : ISerializable
	{
		public abstract string Name { get; set; }
		public abstract string Description { get; set; }
		public abstract int Price { get; set; }
		public abstract int SellingPrice { get; set; }
		public abstract IItem Clone();

		[JsonIgnore]
		public abstract string UUID { get; set; }

		public virtual void Write(Stream s)
		{
			s.WriteString(Name);
			s.WriteString(Description);
			s.WriteInt32(Price);
			s.WriteInt32(SellingPrice);
			s.WriteString(UUID);
		}

		public virtual void Read(Stream s)
		{
			Name = s.ReadString();
			Description = s.ReadString();
			Price = s.ReadInt32();
			SellingPrice = s.ReadInt32();
			UUID = s.ReadString();
		}

		public virtual void ToJson(JsonWriter writer)
		{
			writer.WritePropertyName("Name");
			writer.WriteValue(Name);
			writer.WritePropertyName("Description");
			writer.WriteValue(Description);
			writer.WritePropertyName("Price");
			writer.WriteValue(Price);
			writer.WritePropertyName("SellingPrice");
			writer.WriteValue(SellingPrice);
		}
	}
}
