﻿namespace CommonUtilities.Item
{
	public interface IItemLoader
	{
		IItem GetItem(string id);
	}
}
