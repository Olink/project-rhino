﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	public class Armor : IEquipment
	{
		private string name;
		private string description;
		private int price;
		private int sellingPrice;
		private string uuid;
		private EquipmentSlot slot;

		public override string Name { get { return name; } set { name = value; } }
		public override string Description { get { return description; } set { description = value; } }
		public override int Price { get { return price; } set { price = value; } }
		public override int SellingPrice { get { return sellingPrice; } set { sellingPrice = value; } }
		public override string UUID { get { return uuid; } set { uuid = value; } }
		public EquipmentSlot Slot { get { return slot; } set { slot = value; } }

		public Armor(string n, string d, int p, int sp, EquipmentSlot s)
		{
			name = n;
			description = d;
			price = p;
			sellingPrice = sp;
			slot = s;
		}

		public override IItem Clone()
		{
			var armor = new Armor(name, description, price, sellingPrice, slot);
			return armor;
		}

		public override void Write(Stream s)
		{
			s.WriteString("armor");
			base.Write(s);
			s.WriteString(slot.ToString());
		}

		public override void Read(Stream s)
		{
			base.Read(s);
			EquipmentSlot slot;
			Enum.TryParse(s.ReadString(), out slot);
			Slot = slot;
		}

		public static Armor FromStream(Stream s)
		{
			var name = s.ReadString();
			var desc = s.ReadString();
			var price = s.ReadInt32();
			var selling = s.ReadInt32();
			var uuid = s.ReadString();
			EquipmentSlot slot;
			Enum.TryParse(s.ReadString(), out slot);

			return new Armor(name, desc, price, selling, slot) { UUID = uuid };
		}

		public override string ToString()
		{
			return String.Format("Armor: {0} - {1}.  Sells for {2}, bought for {3}.", name, description, sellingPrice, price);
		}

		public override void ToJson(JsonWriter writer)
		{
			writer.WriteStartObject();
			writer.WritePropertyName("ItemType");
			writer.WriteValue("armor");
			base.ToJson(writer);
			writer.WritePropertyName("Slot");
			writer.WriteValue(slot.ToString());
			writer.WriteEndObject();
		}

		public static Armor FromJson(JsonReader reader)
		{
			reader.Read();
			var name = reader.ReadAsString();
			reader.Read();
			var desc = reader.ReadAsString();
			reader.Read();
			var price = reader.ReadAsInt32();
			reader.Read();
			var selling = reader.ReadAsInt32();
			reader.Read();
			EquipmentSlot slot;
			Enum.TryParse(reader.ReadAsString(), out slot);
			reader.Read();
			return new Armor(name, desc, !price.HasValue ? 0 : price.Value, !selling.HasValue ? 0 : selling.Value, slot);
		}
	}
}