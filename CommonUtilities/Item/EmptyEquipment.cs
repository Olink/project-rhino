using System;
using System.IO;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	public class EmptyEquipment : IEquipment
	{
		private EquipmentSlot m_Slot;

		public override string Name { get; set; }
		public override string Description { get; set; }
		public override int Price { get; set; }
		public override int SellingPrice { get; set; }
		public override string UUID { get; set; }
		public EquipmentSlot Slot { get { return m_Slot; } set { m_Slot = value; } }

		public EmptyEquipment(EquipmentSlot slot)
		{
			m_Slot = slot;
		}

		public override IItem Clone()
		{
			return new EmptyEquipment(Slot);
		}

		public override void Write(Stream s)
		{
			s.WriteString("empty");
			s.WriteString(Slot.ToString());
		}

		public override void Read(Stream s)
		{
			EquipmentSlot slot;
			Enum.TryParse(s.ReadString(), out slot);
			Slot = slot;
		}

		public static EmptyEquipment FromStream(Stream s)
		{
			EquipmentSlot slot;
			Enum.TryParse(s.ReadString(), out slot);

			return new EmptyEquipment(slot);
		}

		public override string ToString()
		{
			return "Empty";
		}

		public override void ToJson(JsonWriter writer)
		{
			writer.WriteStartObject();
			writer.WritePropertyName("ItemType");
			writer.WriteValue("equipmentempty");
			writer.WritePropertyName("Slot");
			writer.WriteValue(Slot.ToString());
			writer.WriteEndObject();
		}

		public static EmptyEquipment FromJson(JsonReader reader)
		{
			reader.Read();
			EquipmentSlot slot;
			Enum.TryParse(reader.ReadAsString(), out slot);
			reader.Read();
			return new EmptyEquipment(slot);
		}
	}
}