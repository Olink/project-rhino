﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CommonUtilities.Concepts;

namespace CommonUtilities.Item
{
	public class Inventory : ISerializable
	{
		public int MaxSize { get; set; }
		public List<IItem> Contents { get; set; } 

		public Inventory()
		{
			Contents = new List<IItem>();
		}

		public Inventory(int slots)
		{
			MaxSize = slots;
			Contents = new List<IItem>();
		}

		public int AvailableRoom()
		{
			return MaxSize - Contents.Count;
		}

		public int AddItem(IItem i)
		{
			if (Contents.Count == MaxSize)
				throw new InvalidOperationException("Inventory is full.");
			Contents.Add(i);
			return Contents.Count - 1;
		}

		public void RemoveItem(IItem i)
		{
			Contents.Remove(i);
		}

		public void Write(Stream s)
		{
			s.WriteInt32(MaxSize);
			s.WriteInt32(Contents.Count);
			foreach(var i in Contents)
				i.Write(s);
		}

		public void Read(Stream s)
		{
			MaxSize = s.ReadInt32();
			var count = s.ReadInt32();
			Contents = new List<IItem>(MaxSize);
			for(int i = 0; i < count; i++)
			{
				AddItem(ItemFactory.GetItem(s));
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder(String.Format("Inventory size {0} / {1}", Contents.Count, MaxSize));
			for(int i = 0; i < Contents.Count; i++)
			{
				builder.Append(String.Format("\n\t{0}: {1}", i + 1, Contents[i].ToString()));
			}

			return builder.ToString();

		}

		public static Inventory DefaultInventory()
		{
			var inventory = new Inventory(10);
			inventory.AddItem(new Weapon("broken shiv", "A broken shiv you found on the ground.", 0, 0, WeaponSize.OneHanded));
			return inventory;
		}
	}
}
