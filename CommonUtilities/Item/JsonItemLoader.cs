﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	public class JsonItemLoader : IItemLoader
	{
		Dictionary<string, IItem> m_ItemStore = new Dictionary<string, IItem>(); 

		public JsonItemLoader(string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
				Weapon w = new Weapon("Sword", "A basic sword", 0, 0, WeaponSize.OneHanded);
				Weapon w2 = new Weapon("Greatsword", "A great sword.", 10, 5, WeaponSize.TwoHanded);
				Armor a = new Armor("Helmet", "A basic helmet", 0, 0, EquipmentSlot.Head);

				using(var writer = new StreamWriter(Path.Combine(path, String.Format("{0}.json", w.Name))))
				{
					writer.WriteLine("weapon");
					writer.Write(JsonConvert.SerializeObject(w, Formatting.Indented));
				}

				using (var writer = new StreamWriter(Path.Combine(path, String.Format("{0}.json", w2.Name))))
				{
					writer.WriteLine("weapon");
					writer.Write(JsonConvert.SerializeObject(w2, Formatting.Indented));
				}

				using (var writer = new StreamWriter(Path.Combine(path, String.Format("{0}.json", a.Name))))
				{
					writer.WriteLine("armor");
					writer.Write(JsonConvert.SerializeObject(a, Formatting.Indented));
				}
			}

			LoadItems(path);
		}

		private void LoadItems(string path)
		{
			foreach(var file in Directory.GetFiles(path))
			{
				using(var reader = new StreamReader(file))
				{
					var type = reader.ReadLine();
					var text = reader.ReadToEnd();
					IItem item = null;
					switch(type)
					{
						case "weapon":
							item = JsonConvert.DeserializeObject<Weapon>(text);
							break;
						case "armor":
							item = JsonConvert.DeserializeObject<Armor>(text);
							break;
						default:
							continue;
					}
					m_ItemStore.Add(item.Name, item);
				}
			}
		}

		public IItem GetItem(string id)
		{
			if (m_ItemStore.ContainsKey(id))
				return m_ItemStore[id].Clone();
			throw new InvalidOperationException("Item not loaded!");
		}
	}
}
