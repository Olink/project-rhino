﻿using System.IO;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	class ItemFactory
	{
		public static IItem GetItem(Stream s)
		{
			var type = s.ReadString();
			switch(type)
			{
				case "consumable":
					return null;
				case "weapon":
					return GetWeapon(s);
				case "armor":
					return GetArmor(s);
				case "empty":
					return GetEmpty(s);
			}
			return null;
		}

		public static IEquipment GetEquipment(Stream s)
		{
			var type = s.ReadString();
			switch (type)
			{
				case "weapon":
					return GetWeapon(s);
				case "armor":
					return GetArmor(s);
				case "empty":
					return GetEmpty(s);
			}
			return null;
		}

		private static Weapon GetWeapon(Stream s)
		{
			return Weapon.FromStream(s);
		}

		private static Armor GetArmor(Stream s)
		{
			return Armor.FromStream(s);
		}

		private static EmptyEquipment GetEmpty(Stream s)
		{
			return EmptyEquipment.FromStream(s);
		}

		public static IItem FromJson(JsonReader reader)
		{
			reader.Read();
			var type = reader.ReadAsString();
			IItem ret = null;
			switch (type)
			{
				case "weapon":
					ret = Weapon.FromJson(reader);
					break;
				case "armor":
					ret = Armor.FromJson(reader);
					break;
				case "equipmentempty":
					ret = EmptyEquipment.FromJson(reader);
					break;
			}

			return ret;
		}
	}
}
