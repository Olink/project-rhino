﻿using System;
using Newtonsoft.Json;

namespace CommonUtilities.Item
{
	class IItemJsonConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var item = value as IItem;
			item.ToJson(writer);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return ItemFactory.FromJson(reader);
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType.IsAssignableFrom(typeof(IItem));
		}
	}
}
