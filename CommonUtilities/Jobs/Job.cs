﻿using System;
using System.Collections.Generic;

namespace CommonUtilities.Jobs
{
	public class Job : ICloneable
	{
		private string name;
		private int level;
		private Dictionary<string, int> req;

		public string Name { get { return name; } }
		public int Level { get { return level; } }
		public Dictionary<string, int> Requirements { get { return req; } }

		public Job()
		{
			req = new Dictionary<string, int>();
		}

		public Job(string n, int l, Dictionary<string, int> r)
		{
			name = n;
			level = l;
			req = r;
		}

		public object Clone()
		{
			var job = new Job(name, level, req);
			return job;
		}
	}
}
