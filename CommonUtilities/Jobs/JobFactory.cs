﻿namespace CommonUtilities.Jobs
{
	public class JobFactory
	{
		private static IJobManager m_JobManager;

		public JobFactory(IJobManager jobm)
		{
			m_JobManager = jobm;
		}

		public static Job GetJob(string job)
		{
			return m_JobManager.GetJob(job);
		}
	}
}
