﻿using System.Collections.Generic;
using System.IO;

namespace CommonUtilities.Jobs
{
	public class JsonJobManager : IJobManager
	{
		public JsonJobManager(string path) : base(path)
		{
			if (!File.Exists(Path.Combine(path, "Squire")))
				File.Create(Path.Combine(path, "Squire"));

			if (!File.Exists(Path.Combine(path, "Apprentice")))
				File.Create(Path.Combine(path, "Apprentice"));

			LoadJobs(path);
		}

		//TODO: Load the files and actually parse jobs
		private void LoadJobs(string path)
		{
			m_Jobs = new Dictionary<string, Job>();
			foreach (var file in Directory.GetFiles(path))
			{
				m_Jobs.Add(Path.GetFileName(file), new Job(Path.GetFileName(file), 1, new Dictionary<string, int>()));
			}
		}
	}
}
