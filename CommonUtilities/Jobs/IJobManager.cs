﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CommonUtilities.Jobs
{
	public abstract class IJobManager
	{
		protected Dictionary<string, Job> m_Jobs;

		public IJobManager(string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		public Job GetJob(string name)
		{
			if (m_Jobs.ContainsKey(name))
				return (Job)m_Jobs[name].Clone();

			throw new InvalidOperationException("Job does not exist.");
		}
	}
}
