﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonUtilities.Concepts
{
	public interface IObserver<T>
	{
		void Notify(T hint);
	}
}
