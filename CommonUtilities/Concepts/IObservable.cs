﻿using System.Security.Cryptography.X509Certificates;

namespace CommonUtilities.Concepts
{
	public interface IObservable<T>
	{
		void Notify(T hint);
		void Subscribe(IObserver<T> observer);
		void Unsubscribe(IObserver<T> observer);
	}
}
