using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace CommonUtilities
{
	public class Utilities
	{
		private static Random rand = new Random();

		private static Dictionary<int, ConsoleColor> colorMaps = new Dictionary<int, ConsoleColor>
		                                                         	{
		                                                         		{0, ConsoleColor.White},
		                                                         		{1, ConsoleColor.Red},
		                                                         		{2, ConsoleColor.Green},
		                                                         		{3, ConsoleColor.Yellow},
		                                                         		{4, ConsoleColor.Blue},
		                                                         		{5, ConsoleColor.Cyan},
		                                                         		{6, ConsoleColor.Magenta},
		                                                         		{7, ConsoleColor.White},
		                                                         		{8, ConsoleColor.DarkCyan},
		                                                         		{9, ConsoleColor.Gray},
		                                                         	};

		[Conditional("DEBUG")]
		public static void PrintToConsole(string message)
		{
			Console.WriteLine(message);
		}

		public static string[] BreakIntoParameters(string input)
		{
			List<string> parameters = new List<string>();

			bool open_quotes = false;
			string current_parameter = "";
			foreach (var c in input)
			{
				if (c == '"' && !open_quotes)
				{
					open_quotes = true;
					continue;
				}
				if (c == '"' && open_quotes)
				{
					open_quotes = false;
					if (!string.IsNullOrEmpty(current_parameter))
					{
						parameters.Add(current_parameter);
						current_parameter = "";
					}
					continue;
				}
				if (c == ' ' && !open_quotes)
				{
					if (!string.IsNullOrEmpty(current_parameter))
					{
						parameters.Add(current_parameter);
						current_parameter = "";
					}
					continue;
				}
				current_parameter += c.ToString();
			}
			if (!string.IsNullOrEmpty(current_parameter))
			{
				parameters.Add(current_parameter);
				current_parameter = "";
			}
			return parameters.ToArray();
		}

		public static void ParseAndOutputChat(string message)
		{
			int number = 0;
			Console.ResetColor();
			for (int i = 0; i < message.Length; i++)
			{
				char c = message[i];

				if ((c == '^') && ((i + 1) < message.Length) && (int.TryParse(message[i + 1].ToString(), out number)))
				{
					Console.ForegroundColor = colorMaps[number];
					i++;
				}
				else
					Console.Write(c);
			}
			//Write a endline
			Console.ResetColor();
			Console.WriteLine();
		}

		public static List<int> RollDice(int die)
		{
			List<int> roll = new List<int>(die);
			for (int i = 0; i < die; i++)
			{
				int r = rand.Next(1, 7);
				roll.Add(r);
			}
			return roll;
		}

		public static string RandomString(int min, int max)
		{
			StringBuilder builder = new StringBuilder();
			char ch;
			int size = rand.Next(min, max + 1);
			for (int i = 0; i < size; i++)
			{
				ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26*rand.NextDouble() + 65)));
				builder.Append(ch);
			}

			return builder.ToString();
		}
	}

	public static class ListShufflerExtensionMethods
	{
		//for getting random values
		private static Random _rnd = new Random();

		/// <summary>
		/// Shuffles the contents of a list
		/// </summary>
		/// <typeparam name="T">The type of the list to sort</typeparam>
		/// <param name="listToShuffle">The list to shuffle</param>
		/// <param name="numberOfTimesToShuffle">How many times to shuffle the list
		/// by default this is 5 times</param>
		public static void Shuffle<T>(this List<T> listToShuffle, int numberOfTimesToShuffle = 5)
		{
			//make a new list of the wanted type
			List<T> newList = new List<T>();

			//for each time we want to shuffle
			for (int i = 0; i < numberOfTimesToShuffle; i++)
			{
				//while there are still items in our list
				while (listToShuffle.Count > 0)
				{
					//get a random number within the list
					int index = _rnd.Next(listToShuffle.Count);

					//add the item at that position to the new list
					newList.Add(listToShuffle[index]);

					//and remove it from the old list
					listToShuffle.RemoveAt(index);
				}

				//then copy all the items back in the old list again
				listToShuffle.AddRange(newList);

				//and clear the new list
				//to make ready for next shuffling
				newList.Clear();
			}
		}
	}
}
