﻿using System;

namespace CommonUtilities.Crypto
{
	public abstract class ICryptoProvider
	{
		public abstract String ComputeHash(String s);
		public abstract String SaltString(String s);
		protected abstract String Salt { get; }
	}
}
