﻿using System.Security.Cryptography;
using System.Text;

namespace CommonUtilities.Crypto
{
	public class BasicMD5 : ICryptoProvider
	{
		public override string ComputeHash(string s)
		{
			MD5 md5 = System.Security.Cryptography.MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(s);
			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				sb.Append(hash[i].ToString("x2"));
			}
			return sb.ToString();
		}

		public override string SaltString(string s)
		{
			return ComputeHash(s + Salt);
		}

		protected override string Salt { get { return ""; } }
	}
}
