﻿using System.Collections.Generic;
using System.Linq;
using CommonUtilities.Messages;

namespace CommonUtilities.Players
{
	public class PlayerManager<T> : IPlayerManager<T> where T : IPlayer
	{
		private List<T> players = new List<T>(); 
		public virtual void AddPlayer(T ply)
		{
			players.Add(ply);
		}

		public void RemovePlayer(T ply)
		{
			players.Remove(ply);
		}

		public IList<T> GetPlayers()
		{
			return players.ToList();
		}

		public T FindPlayer(string uuid)
		{
			return players.FirstOrDefault(p => p.UUID == uuid);
		}

		public T FindPlayer(IReceiver connection)
		{
			return players.FirstOrDefault(p => p.Connection.Equals(connection));
		}

		public string FindUniqueUUID()
		{
			string uuid = System.Guid.NewGuid().ToString();
			while (players.Any(u => u.UUID == uuid))
				uuid = System.Guid.NewGuid().ToString();
			return uuid;
		}

		public IPlayer FindPlayerByName(string opponent)
		{
			return players.FirstOrDefault(p => p.Name == opponent);
		}
	}
}
