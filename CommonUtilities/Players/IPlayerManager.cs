﻿using System.Collections.Generic;
using CommonUtilities.Messages;

namespace CommonUtilities.Players
{
	public interface IPlayerManager<T> where T : IPlayer
	{
		void AddPlayer(T ply);
		void RemovePlayer(T ply);
		IList<T> GetPlayers();
		T FindPlayer(string uuid);
		T FindPlayer(IReceiver connection);
	}
}
