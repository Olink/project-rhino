﻿using CommonUtilities.Messages;

namespace CommonUtilities.Players
{
	public interface IPlayer
	{
		string UUID { get; set; }
		string Name { get; }
		IReceiver Connection { get; }
		
		void SendMessage(IMessage msg);
	}
}
