using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CommonUtilities
{
    public enum LogLevels
    {
        Verbose = 1, //yellow
        Critical = 2, //red
        Warning = 4, //orange
        Debug = 8, //aqua
        Notification = 16, //green
        None = 32 //default
    }

    public class Logger
    {
        private string file_path;
        private int log_level = (int)LogLevels.None;
        private object lock_obj;

        public static Dictionary<LogLevels, ConsoleColor> colors = new Dictionary<LogLevels, ConsoleColor>()
            {
                {LogLevels.Verbose, ConsoleColor.Yellow},
                {LogLevels.Critical, ConsoleColor.Red},
                {LogLevels.Warning, ConsoleColor.Magenta},
                {LogLevels.Debug, ConsoleColor.Cyan},
                {LogLevels.Notification, ConsoleColor.Green},
                {LogLevels.None, ConsoleColor.White}
            };

        public Logger(string path)
        {
            file_path = path;
            lock_obj = new object();
        }

        public void SetLogLevel(LogLevels lvl)
        {
            log_level = log_level | (int)lvl;
        }

        public void SetLogLevel(int lvl)
        {
            log_level = log_level | lvl;
        }

        public bool DisplayLog(LogLevels lvl)
        {
            int masked = log_level & (int)lvl;
            return (masked == (int)lvl);
        }

        public void Verbose(string log)
        {
            Log(log, LogLevels.Verbose);
        }

        public void Critical(string log)
        {
            Log(log, LogLevels.Critical);
        }

        public void Warn(string log)
        {
            Log(log, LogLevels.Warning);
        }

        public void Debug(string log)
        {
            Log(log, LogLevels.Debug);
        }

        public void Notify(string log)
        {
            Log(log, LogLevels.Notification);
        }

        public void Log(string log, LogLevels lvl)
        {
            if (!DisplayLog(lvl)) return;
            lock (lock_obj)
            {
                var trace = new StackTrace();
                var frame = trace.GetFrame(2);
                var caller = "";
                if (frame != null && frame.GetMethod().DeclaringType != null)
                    caller = string.Format("{0}::{1}", frame.GetMethod().DeclaringType.Name, frame.GetMethod().Name);
                string response = string.Format("[{0}]({1}): {2}", Enum.GetName(typeof (LogLevels), lvl), caller, log);
                Console.ForegroundColor = colors[lvl];
                Console.WriteLine(response);
                Console.ResetColor();
                /*File.AppendAllText(file_path, response + "\n");*/
            }
        }
    }
}