﻿using System.Collections.Generic;

namespace CommonUtilities.Attributes
{
	public class Attribute : BaseAttribute
	{
		private List<RawAttributeBonus> rawBonuses = new List<RawAttributeBonus>();
		private List<FinalAttributeBonus> finalBonuses = new List<FinalAttributeBonus>();

		public Attribute() { }
		public Attribute(int n, int m) : base(n,m) {}

		public void AddAttributeBonus(RawAttributeBonus bonus)
		{
			rawBonuses.Add(bonus);
		}

		public void AddAttributeBonus(FinalAttributeBonus bonus)
		{
			finalBonuses.Add(bonus);
		}

		public void RemoveAttributeBonus(RawAttributeBonus bonus)
		{
			rawBonuses.Remove(bonus);
		}

		public void RemoveAttributeBonus(FinalAttributeBonus bonus)
		{
			finalBonuses.Remove(bonus);
		}

		private float CalculateRawValue()
		{
			float finalValueBonus = BaseValue;
			float finalMultiplier = BaseMultiplier;

			foreach (RawAttributeBonus bonus in rawBonuses)
			{
				finalValueBonus += bonus.BaseValue;
				finalMultiplier += bonus.BaseMultiplier;
			}

			return finalValueBonus * (1 + finalMultiplier);
		}

		public float CalculateValue()
		{
			float rawValue = CalculateRawValue();
			float finalValue = 0;
			float finalMultiplier = 0;

			foreach (FinalAttributeBonus bonus in finalBonuses)
			{
				finalValue += bonus.BaseValue;
				finalMultiplier += bonus.BaseMultiplier;
			}

			return (rawValue + finalValue) * (1 + finalMultiplier);
		}
	}
}
