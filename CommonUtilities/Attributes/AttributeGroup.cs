﻿namespace CommonUtilities.Attributes
{
	public class AttributeGroup
	{
		public Attribute HP { get; set; }
		public Attribute MP { get; set; }
		public Attribute PhysicalAttack { get; set; }
		public Attribute MagicalAttack { get; set; }
		public Attribute PhysicalDefense { get; set; }
		public Attribute MagicalDefense { get; set; }
		public Attribute Movement { get; set; }
		public Attribute Dexterity { get; set; }
		public Attribute Agility { get; set; }

		public AttributeGroup()
		{
			HP = new Attribute(40, 0);
			MP = new Attribute(0, 0);
			PhysicalAttack = new Attribute(10, 0);
			MagicalAttack = new Attribute(0, 0);
			PhysicalDefense = new Attribute(5, 0);
			MagicalDefense = new Attribute(0, 0);
			Movement = new Attribute(3, 0);
			Dexterity = new Attribute(10, 0);
			Agility = new Attribute(6, 0);
		}
	}
}
