namespace CommonUtilities.Attributes
{
	public class BaseAttribute
	{
		public int BaseValue { get; set; }
		public float BaseMultiplier { get; set; }
		
		public BaseAttribute()
		{
			BaseValue = 0;
			BaseMultiplier = 0;
		}
		
		public BaseAttribute(int n, float m)
		{
			BaseValue = n;
			BaseMultiplier = m;
		}
	}
}
