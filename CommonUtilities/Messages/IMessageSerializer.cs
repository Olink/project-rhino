﻿using System.IO;

namespace CommonUtilities.Messages
{
    public interface IServerMessageSerializer
    {
        void Serialize(IMessage message, Stream output);
        IMessage Deserialize(Stream input);
    }
    public interface IClientMessageSerializer
    {
        void Serialize(IMessage message, Stream output);
        IMessage Deserialize(Stream input);
    }
}
