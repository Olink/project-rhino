﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CommonUtilities.Messages
{
    public class ServerMessageSerializer : IServerMessageSerializer
    {
        static readonly Dictionary<ServerMessageIds, Type> s_MessageTypes = new Dictionary<ServerMessageIds, Type>(); 

        public ServerMessageSerializer()
        {
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
            {
                var attr = type.GetCustomAttributes(typeof(ServerMessageIdAttribute), false).FirstOrDefault() as ServerMessageIdAttribute;
                if (attr != null)
                    s_MessageTypes.Add(attr.Id, type);
            }
        }

        private ServerMessageIdAttribute GetIdAttribute(Type type)
        {
            return type.GetCustomAttributes(typeof(ServerMessageIdAttribute), false).FirstOrDefault() as ServerMessageIdAttribute;
        }

        public void Serialize(IMessage message, Stream output)
        {
            var attr = GetIdAttribute(message.GetType());
            if (attr == null)
                throw new NotSupportedException("Tried to serialize a message without an id");
            output.WriteByte((byte)attr.Id);
            message.Write(output);
        }

        public IMessage Deserialize(Stream input)
        {
            var id = input.ReadByte();
            var type = s_MessageTypes[(ServerMessageIds)id];
            var message = (IMessage)Activator.CreateInstance(type);
            message.Read(input);
            return message;
        }
    }
}
