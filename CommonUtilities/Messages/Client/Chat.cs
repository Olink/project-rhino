namespace CommonUtilities.Messages.Client
{
    [ClientMessageId(ClientMessageIds.Chat)]
    public class Chat : GenericMessage
    {
        public string Message { get; set; }
		public string UUID { get; set; }

        public Chat() { }

        public Chat(string message, string uuid)
        {
            Message = message;
        	UUID = uuid;
        }
    }
}
