﻿using System;

namespace CommonUtilities.Messages.Client
{
	[ClientMessageId(ClientMessageIds.JoinChatChannel)]
	class JoinChatRoomRequest : GenericMessage
	{
		public JoinChatRoomRequest(){}
		public JoinChatRoomRequest(string uuid)
		{
			UUID = uuid;
		}

		public String UUID { get; set; }
	}
}
