﻿namespace CommonUtilities.Messages.Client
{
    [ClientMessageId(ClientMessageIds.LoginRequest)]
    public class LoginRequest : GenericMessage
    {
        public string Name { get; set; }
        public string Password { get; set; }

        public LoginRequest() { }

        public LoginRequest(string name, string password)
        {
            Name = name;
            Password = password;
        }
    }
}
