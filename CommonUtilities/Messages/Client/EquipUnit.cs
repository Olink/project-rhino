﻿using System;
using System.IO;
using CommonUtilities.Item;

namespace CommonUtilities.Messages.Client
{
	[ClientMessageId(ClientMessageIds.EquipUnitWithItem)]
	public class EquipUnit : IMessage
	{
		public EquipUnit() {}
		public EquipUnit(int item, int unit, EquipmentSlot slot)
		{
			ItemIndex = item;
			UnitIndex = unit;
			Slot = slot;
		}

		public int ItemIndex { get; set; }
		public int UnitIndex { get; set; }
		public EquipmentSlot Slot { get; set; }

		public void Write(Stream s)
		{
			s.WriteInt32(ItemIndex);
			s.WriteInt32(UnitIndex);
			s.WriteString(Slot.ToString());
		}

		public void Read(Stream s)
		{
			ItemIndex = s.ReadInt32();
			UnitIndex = s.ReadInt32();
			EquipmentSlot slot;
			Enum.TryParse(s.ReadString(), out slot);
			Slot = slot;
		}
	}
}
