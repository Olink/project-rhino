﻿using System;

namespace CommonUtilities.Messages.Client
{
	[ClientMessageId(ClientMessageIds.RequestBattle)]
	public class RequestBattle : GenericMessage
	{
		public RequestBattle() {}
		public RequestBattle(string o)
		{
			Opponent = o;
		}

		public String Opponent { get; set; }
	}
}
