﻿using System;
using System.IO;

namespace CommonUtilities.Messages.Client
{
	[ClientMessageId(ClientMessageIds.BattleAction)]
	public class BattleAction : IMessage
	{
		public BattleAction() {}
		public BattleAction(String uuid, BattleActionEnum action, String target)
		{
			UUID = uuid;
			Action = action;
			Target = target;
		}

		public String UUID { get; set; }
		public BattleActionEnum Action { get; set; }
		public string Target { get; set; }
		public void Write(Stream s)
		{
			s.WriteString(UUID);
			s.WriteString(Action.ToString());
			s.WriteString(Target);
		}

		public void Read(Stream s)
		{
			UUID = s.ReadString();
			BattleActionEnum action;
			Enum.TryParse(s.ReadString(), out action);
			Action = action;
			Target = s.ReadString();
		}
	}

	public enum BattleActionEnum
	{
		Wait,
		Attack,
		Defend
	}
}
