﻿namespace CommonUtilities.Messages.Client
{
	[ClientMessageId(ClientMessageIds.RegisterRequest)]
	public class RegisterRequest : GenericMessage
	{
		public string Name { get; set; }
		public string Password { get; set; }

		public RegisterRequest() { }

		public RegisterRequest(string name, string password)
		{
			Name = name;
			Password = password;
		}
	}
}
