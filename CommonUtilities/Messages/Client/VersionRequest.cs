﻿using System.Diagnostics;
using System.IO;

namespace CommonUtilities.Messages.Client
{
    [ClientMessageIdAttribute(ClientMessageIds.VersionRequest)]
    public class VersionRequest : GenericMessage
    {
        public VersionRequest() { }
        public VersionRequest(string version)
        {
            Version = version;
        }

        public string Version { get; set; }
    }

    public class MessageSerializationTest
    {
        public void Test()
        {
            var req = new VersionRequest("abc123");
            var ms = new MemoryStream();
            req.Write(ms);
            ms.Position = 0;
            var deReq = new VersionRequest("");
            deReq.Read(ms);
            Debug.Assert(deReq.Version == req.Version);
        }
    }
}
