﻿using System;
using System.Collections.Generic;

namespace CommonUtilities.Messages
{
    public interface IMessenger
    {
        IList<IReceiver> Receivers { get; }
        void SendMessage(IMessage msg, IReceiver receiver);
        void SendAllMessage(IMessage msg);
        event EventHandler<ReceiverEventArgs> ReceiverAdded;
        event EventHandler<ReceiverEventArgs> ReceiverRemoved;
        event EventHandler<MessageReceivedEventArgs> MessageReceived;
    }
    public interface IReceiver
    {

    }
    public class ReceiverEventArgs : EventArgs
    {
        IReceiver m_Receiver;

        public ReceiverEventArgs(IReceiver receiver)
        {
            m_Receiver = receiver;
        }

        public IReceiver Receiver
        {
            get { return m_Receiver; }
        }
    }
    public class MessageReceivedEventArgs : EventArgs
    {
        readonly IMessenger m_Messenger;
        readonly IMessage m_Message;
        readonly IReceiver m_Sender;
        public MessageReceivedEventArgs(IMessage message, IReceiver sender, IMessenger messenger)
        {
            m_Message = message;
            m_Sender = sender;
            m_Messenger = messenger;
        }

        public IMessage Message
        {
            get { return m_Message; }
        }

        public IReceiver Sender
        {
            get { return m_Sender; }
        }

        public IMessenger Messenger
        {
            get { return m_Messenger; }
        }
    }
}
