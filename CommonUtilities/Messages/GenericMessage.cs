﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CommonUtilities.Messages
{
    public abstract class GenericMessage : IMessage
    {
        public virtual void Write(Stream s)
        {
            foreach (var prop in GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                s.WriteString(prop.Name);
                Action<Stream, object> func;
                if (!s_WriteFuncs.TryGetValue(prop.PropertyType, out func))
                    throw new NotSupportedException(string.Format("Type {0} is not supported for serialization.", prop.PropertyType));
                func(s, prop.GetValue(this, null));
            }
        }

        public virtual void Read(Stream s)
        {
            var props = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(p => p.Name, p => p);

            foreach (var t in props)
            {
                var propName = s.ReadString();

                PropertyInfo prop;
                if (!props.TryGetValue(propName, out prop))
                    throw new NotSupportedException(string.Format("Property {0} is not found on type {1}.", propName, GetType()));

                Func<Stream, object> func;
                if (!s_ReadFuncs.TryGetValue(prop.PropertyType, out func))
                    throw new NotSupportedException(string.Format("Type {0} is not supported for serialization.", prop.PropertyType));

                prop.SetValue(this, func(s), null);
            }
        }

        static readonly Dictionary<Type, Action<Stream, object>> s_WriteFuncs = new Dictionary<Type, Action<Stream, object>>()
        {
            {typeof(bool), (s, o) => s.WriteBoolean((bool)o)},
            {typeof(byte), (s, o) => s.WriteInt8((byte)o)},
            {typeof(Int16), (bw, data) => bw.WriteInt16((Int16)data) },
            {typeof(Int32), (bw, data) => bw.WriteInt32((Int32)data) },
            {typeof(Int64), (bw, data) => bw.WriteInt64((Int64)data) },
            {typeof(Single), (bw, data) => bw.WriteSingle((Single)data) },
            {typeof(Double), (bw, data) => bw.WriteDouble((Double)data) },
            {typeof(byte[]), (s, o) => s.WriteBytesWithLength((byte[])o)},
            {typeof(string), (s, o) => s.WriteString((string)o)},
        };
        static readonly Dictionary<Type, Func<Stream, object>> s_ReadFuncs = new Dictionary<Type, Func<Stream, object>>()
        {
            {typeof(bool), s => s.ReadBoolean()},
            {typeof(byte), s => s.ReadInt8()},
            {typeof(Int16), br => br.ReadInt16() },
            {typeof(Int32), br => br.ReadInt32() },
            {typeof(Int64), br => br.ReadInt64() },
            {typeof(Single), br => br.ReadSingle() },
            {typeof(Double), br => br.ReadDouble() },
            {typeof(byte[]), s => s.ReadBytesWithLength()},
            {typeof(string), s => s.ReadString()},
        };
    }
}
