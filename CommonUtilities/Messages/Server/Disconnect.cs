﻿using System;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.Disconnect)]
	public class Disconnect : GenericMessage
	{
		public String Reason { get; set; }

		public Disconnect() { }
		public Disconnect(String reason)
		{
			Reason = reason;
		}
	}
}