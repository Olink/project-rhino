﻿namespace CommonUtilities.Messages.Server
{
    [ServerMessageId(ServerMessageIds.VersionResponse)]
    public class VersionResponse : GenericMessage
    {
        public VersionResponse() { }
        public VersionResponse(bool allowed, string version)
        {
        	Allowed = allowed;
            Version = version;
        }

		public bool Allowed { get; set; }
        public string Version { get; set; }
    }
}
