﻿using System;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.LeaveServer)]
	public class ServerLeave : GenericMessage
	{
		public ServerLeave() {}
		public ServerLeave(String uuid)
		{
			UUID = uuid;
		}

		public string UUID { get; set; }
	}
}
