﻿using System.IO;
using CommonUtilities.Messaging;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.ChatChannelInitialJoin)]
	public class PlayerJoinChatroom : IMessage
	{
		public PlayerJoinChatroom() { }
		public PlayerJoinChatroom(DummyChatroom room)
		{
			Room = room;
		}

		public DummyChatroom Room { get; set; }

		public void Write(Stream s)
		{
			Room.Write(s);
		}

		public void Read(Stream s)
		{
			Room = new DummyChatroom();
			Room.Read(s);
		}
	}
}

