﻿using System.IO;
using CommonUtilities.Item;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.Inventory)]
	public class InventoryResponse : IMessage
	{
		public InventoryResponse(){}
		public InventoryResponse(Inventory i)
		{
			Inventory = i;
		}

		public Inventory Inventory { get; set; }

		public void Write(Stream s)
		{
			Inventory.Write(s);
		}

		public void Read(Stream s)
		{
			Inventory = new Inventory();
			Inventory.Read(s);
		}
	}
}
