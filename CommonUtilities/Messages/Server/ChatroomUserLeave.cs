﻿using System;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.ChatChannelLeave)]
	public class ChatroomUserLeave : GenericMessage
	{
		public ChatroomUserLeave(){}
		public ChatroomUserLeave(string uuid, string ply)
		{
			UUID = uuid;
			PlayerUUID = ply;
		}

		public string UUID { get; set; }
		public String PlayerUUID { get; set; }
	}
}
