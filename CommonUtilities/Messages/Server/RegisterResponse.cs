﻿using System;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.RegisterResponse)]
	public class RegisterResponse : GenericMessage
	{
		public bool Success { get; set; }
		public String Error { get; set; }

		public RegisterResponse() { }
		public RegisterResponse(bool succ, String error)
		{
			Success = succ;
			Error = error;
		}
	}
}