﻿using System.IO;
using CommonUtilities.Unit;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.RosterResponse)]
	public class RosterResponse : IMessage
	{
		public Roster Roster { get; set; }

		public RosterResponse()
		{
			
		}

		public RosterResponse(Roster r)
		{
			Roster = r;
		}

		public void Write(Stream s)
		{
			Roster.Write(s);
		}

		public void Read(Stream s)
		{
			Roster = new Roster();
			Roster.Read(s);
		}
	}
}
