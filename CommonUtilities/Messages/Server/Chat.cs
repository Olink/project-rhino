using System;

namespace CommonUtilities.Messages.Server
{
    [ServerMessageId(ServerMessageIds.Chat)]
    public class Chat : GenericMessage
    {
        public String Message { get; set; }
    	public String UUID { get; set; }

        public Chat() { }
        public Chat(String message, String uuid)
        {
            Message = message;
        	UUID = uuid;
        }
    }
}