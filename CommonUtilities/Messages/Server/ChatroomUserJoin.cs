﻿using System;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.ChatChannelJoin)]
	public class ChatroomUserJoin : GenericMessage
	{
		public ChatroomUserJoin(){}
		public ChatroomUserJoin(string uuid, string ply)
		{
			UUID = uuid;
			PlayerUUID = ply;
		}

		public string UUID { get; set; }
		public String PlayerUUID { get; set; }
	}
}
