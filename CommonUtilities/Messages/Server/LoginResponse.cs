﻿namespace CommonUtilities.Messages.Server
{
    [ServerMessageId(ServerMessageIds.LoginResponse)]
    public class LoginResponse : GenericMessage
    {
        public bool Success { get; set; }
        public string Error { get; set; }

        public LoginResponse() { }
        public LoginResponse(bool success, string err)
        {
            Success = success;
            Error = err;
        }
    }
}
