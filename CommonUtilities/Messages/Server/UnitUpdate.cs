﻿using System.IO;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.Unit)]
	public class UnitUpdate : IMessage
	{
		public UnitUpdate() {}

		public UnitUpdate(int index, Unit.Unit unit)
		{
			Index = index;
			Unit = unit;
		}

		public int Index { get; set; }
		public Unit.Unit Unit { get; set; }

		public void Write(Stream s)
		{
			s.WriteInt32(Index);
			Unit.Write(s);
		}

		public void Read(Stream s)
		{
			Index = s.ReadInt32();
			Unit = new Unit.Unit();
			Unit.Read(s);
		}
	}
}
