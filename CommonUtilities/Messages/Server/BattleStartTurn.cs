﻿namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.BattleStartTurn)]
	public class BattleStartTurn : GenericMessage
	{
		public BattleStartTurn() {}
		public BattleStartTurn(string u)
		{
			UnitUUID = u;
		}

		public string UnitUUID { get; set; }
	}
}
