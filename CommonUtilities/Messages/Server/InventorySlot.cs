﻿using System.IO;
using CommonUtilities.Item;

namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.InventorySlot)]
	public class InventorySlot : IMessage
	{
		public InventorySlot() {}
		public InventorySlot(int index, IItem it)
		{
			Index = index;
			Item = it;
		}

		public int Index { get; set; }
		public IItem Item { get; set; }

		public void Write(Stream s)
		{
			s.WriteInt32(Index);
			Item.Write(s);
		}

		public void Read(Stream s)
		{
			Index = s.ReadInt32();
			Item = ItemFactory.GetItem(s);
		}
	}
}