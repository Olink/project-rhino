﻿namespace CommonUtilities.Messages.Server
{
	[ServerMessageId(ServerMessageIds.JoinServer)]
	public class ServerJoin : GenericMessage
	{
		public ServerJoin() {}
		public ServerJoin(string uuid, string name)
		{
			UUID = uuid;
			Name = name;
		}

		public string UUID { get; set; }
		public string Name { get; set; }
	}
}
