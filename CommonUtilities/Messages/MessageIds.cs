using System;

namespace CommonUtilities.Messages
{
    /// <summary>
    /// All the packets that are sent from the client
    /// Stucture for each packet is shown in summary, with each data type required to make it a valid packet
    /// Every packet starts with: PacketType - (Int32)
    /// </summary>
    internal enum ClientMessageIds : byte
    {
        /// <summary>
        /// Client -> Server
        /// <para>Client Version - (String)</para>
        /// </summary>
        VersionRequest = 1,

        /// <summary>
        /// Client -> Server
        /// <para>Name - string</para>
        /// <para>Password - string</para>
        /// </summary>
		RegisterRequest = 2,

		/// <summary>
		/// Client -> Server
		/// <para>Name - string</para>
		/// <para>Password - string</para>
		/// </summary>
		LoginRequest,

        /// <summary>
        /// Client -> Server
        /// <para>Message - string</para>
		/// <para>UUID - Chat room id</para>
        /// </summary>
        Chat,

		/// <summary>
		/// Client -> Server
		/// <para>empty - Can only request your roster</para>
		/// </summary>
		RequestRoster,

		/// <summary>
		/// Client -> Server
		/// <para>UUID - string</para>
		/// <para>Password - string</para>
		/// </summary>
		JoinChatChannel,

		/// <summary>
		/// Client -> Server
		/// <para>Unit ID - int</para>
		/// <para>Inventory ID - int</para>
		/// <para>Slot - EquipmentSlot</para>
		/// </summary>
		EquipUnitWithItem,

		RequestBattle,

		BattleAction,
    }
    /// <summary>
    /// All the packets that are sent by the server
    /// Stucture for each packet is shown in summary, with each data type required to make it a valid packet
    /// Every packet starts with: PacketType - (Int32)
    /// </summary>
    internal enum ServerMessageIds : byte
    {
        /// <summary>
        /// Server -> Client
        /// <para>Same Version - (Boolean)</para>
        /// <para>Server Version - (String)</para>
        /// </summary>
        VersionResponse = 1,

        /// <summary>
        /// Server -> Client
        /// <para>Success - bool</para>
        /// <para>Error - string</para>
        /// </summary>
        RegisterResponse = 2,

		/// <summary>
		/// Server -> Client
		/// <para>Success - bool</para>
		/// <para>Error - string</para>
		/// </summary>
		LoginResponse,

        /// <summary>
        /// Server -> All Clients
        /// <para>Message - string</para>
		/// <para>UUID - Chat room id</para>
        /// </summary>
        Chat,

		/// <summary>
		/// Server -> Client
		/// <para>Roster - roster</para>
		/// </summary>
		RosterResponse,

		/// <summary>
		/// Server -> Client
		/// <para>ChatRoom - ChatRoom</para>
		/// </summary>
		ChatChannelInitialJoin,

		/// <summary>
		/// Server -> Client
		/// <para>UUID - string channel uuid</para>
		/// <para>UUID - string player uuid</para>
		/// </summary>
		ChatChannelJoin,

		/// <summary>
		/// Server -> Client
		/// <para>UUID - string channel uuid</para>
		/// <para>UUID - string player uuid</para>
		/// </summary>
		ChatChannelLeave,

		/// <summary>
		/// Server -> Client
		/// <para>Name - string</para>
		/// <para>UUID - string</para>
		/// </summary>
		JoinServer,

		/// <summary>
		/// Server -> Client
		/// <para>UUID - string</para>
		/// </summary>
		LeaveServer,

		/// <summary>
		/// Server -> Client
		/// <para>Inventory - inventory</para>
		/// </summary>
		Inventory,

		/// <summary>
		/// Server -> Client
		/// <para>Slot - int</para>
		/// <item>Item - IItem</item>
		/// </summary>
		InventorySlot,

		/// <summary>
		/// Server -> Client
		/// <para>Slot - int</para>
		/// <para>Unit - unit</para>
		/// </summary>
		Unit,

		BattleStartTurn,

        /// <summary>
        /// Server -> Client
        /// <para>Reason - string</para>
        /// </summary>
        Disconnect,
    }

    internal class ClientMessageIdAttribute : Attribute
    {
        readonly ClientMessageIds m_Id;

        public ClientMessageIdAttribute(ClientMessageIds id)
        {
            m_Id = id;
        }

        public ClientMessageIds Id
        {
            get { return m_Id; }
        }
    }
    internal class ServerMessageIdAttribute : Attribute
    {
        readonly ServerMessageIds m_Id;

        public ServerMessageIdAttribute(ServerMessageIds id)
        {
            m_Id = id;
        }

        public ServerMessageIds Id
        {
            get { return m_Id; }
        }
    }
}
