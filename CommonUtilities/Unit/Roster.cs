using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CommonUtilities.Concepts;
using CommonUtilities.Jobs;

namespace CommonUtilities.Unit
{
	public class Roster : ISerializable
	{
		public int MaxSlots { get; set; }
		public List<Unit> Units { get; set; }
		
		public static Roster DefaultRoster()
		{
			var r = new Roster();
			r.MaxSlots = 4;
			r.Units = new List<Unit>();
			r.Units.Add(new Unit(){Name = "Bob", Job = JobFactory.GetJob("Squire"), Male = true});
			r.Units.Add(new Unit() { Name = "Joe", Job = JobFactory.GetJob("Apprentice"), Male = true });
			r.Units.Add(new Unit() { Name = "Sue", Job = JobFactory.GetJob("Squire"), Male = false });
			r.Units.Add(new Unit() { Name = "Sally", Job = JobFactory.GetJob("Apprentice"), Male = false });
			return r;
		}
		
		public Roster()
		{
			MaxSlots = 0;
			Units = new List<Unit>();
		}
		
		public Roster(int n)
		{
			MaxSlots = n;
			Units = new List<Unit>();
		}

		public void Write(Stream s)
		{
			s.WriteInt32(MaxSlots);
			s.WriteInt32(Units.Count);
			foreach(var u in Units)
				u.Write(s);
		}

		public void Read(Stream s)
		{
			MaxSlots = s.ReadInt32();
			var count = s.ReadInt32();
			Units = new List<Unit>(count);
			for (int i = 0; i < count; i++)
			{
				var u = new Unit();
				u.Read(s);
				Units.Add(u);
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder(String.Format("Info: Roster size: {0} / {1}", Units.Count, MaxSlots));
			int i = 1;
			foreach (var u in Units)
			{
				builder.Append(String.Format("\n\tInfo: Unit {0}: {1}", i++, u.ToString()));
			}

			return builder.ToString();
		}
	}
}
