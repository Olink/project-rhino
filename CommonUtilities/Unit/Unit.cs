using System;
using System.IO;
using CommonUtilities.Attributes;
using CommonUtilities.Concepts;
using CommonUtilities.Item;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using CommonUtilities.Jobs;

namespace CommonUtilities.Unit
{
	public class Unit : ISerializable
	{
		public Job Job{ get; set; }
		public bool Male { get; set; }
		public string Name { get; set; }

		public EquipmentManager m_Equipment;

		[JsonIgnore]
		public AttributeGroup Attributes { get; set; }

		public Unit()
		{
			m_Equipment = new EquipmentManager();
			Attributes = new AttributeGroup();
		}

		public Unit(string job, bool man, string name)
		{
			Job = JobFactory.GetJob(job);
			Male = man;
			Name = name;
			Attributes = new AttributeGroup();
			if(Job.Name =="Apprentice")
			{
				Attributes.PhysicalAttack.BaseValue-=2;
				Attributes.PhysicalDefense.BaseValue -= 2;
				Attributes.Agility.BaseValue += 2;
			}
		}

		public List<IEquipment> EquipItem(Weapon item, EquipmentSlot slot)
		{
			var removedGear = new List<IEquipment>();
			if (item.Size == WeaponSize.TwoHanded ||
				(m_Equipment.RightHand is Weapon && ((Weapon)m_Equipment.RightHand).Size == WeaponSize.TwoHanded) ||
				(m_Equipment.LeftHand is Weapon && ((Weapon)m_Equipment.LeftHand).Size == WeaponSize.TwoHanded))
			{
				removedGear.Add(m_Equipment.RemoveItem(EquipmentSlot.RightHand));
				removedGear.Add(m_Equipment.RemoveItem(EquipmentSlot.LeftHand));
			}
			else
			{
				removedGear.Add(m_Equipment.RemoveItem(slot));
			}

			m_Equipment.EquipItem(item, slot);

			return removedGear;
		}

		public List<IEquipment> EquipItem(Armor item, EquipmentSlot slot)
		{
			var removedGear = new List<IEquipment>();
			removedGear.Add(m_Equipment.RemoveItem(slot));

			m_Equipment.EquipItem(item, slot);

			return removedGear;
		}

		public List<IEquipment> EquipItem(EmptyEquipment item, EquipmentSlot slot)
		{
			return new List<IEquipment>(){m_Equipment.RemoveItem(slot)};
		}

		public IEquipment RemoveItem(EquipmentSlot slot)
		{
			return m_Equipment.RemoveItem(slot);
		}

		public void Write(Stream s)
		{
			s.WriteString(Job.Name);
			Console.WriteLine(Job.Name);
			s.WriteBoolean(Male);
			s.WriteString(Name);
			m_Equipment.Write(s);
		}

		public void Read(Stream s)
		{
			var data = s.ReadString();
			Console.WriteLine(data);
			Job = new Job(data, 1, new Dictionary<string, int>());
			Male = s.ReadBoolean();
			Name = s.ReadString();
			m_Equipment = new EquipmentManager();
			m_Equipment.Read(s);
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder(String.Format("{0} the {1} {2} a male.", Name, Job.Name, Male ? "is" : "is not"));
			builder.Append(String.Format("\n\t\tHead: {0}", m_Equipment.Head is EmptyEquipment ? "None" : m_Equipment.Head.Name));
			builder.Append(String.Format("\n\t\tBody: {0}", m_Equipment.Body is EmptyEquipment ? "None" : m_Equipment.Body.Name));
			builder.Append(String.Format("\n\t\tArm: {0}", m_Equipment.Leg is EmptyEquipment ? "None" : m_Equipment.Leg.Name));
			builder.Append(String.Format("\n\t\tLeg: {0}", m_Equipment.Arm is EmptyEquipment ? "None" : m_Equipment.Arm.Name));
			builder.Append(String.Format("\n\t\tAccessory: {0}", m_Equipment.Accessory is EmptyEquipment ? "None" : m_Equipment.Accessory.Name));
			
			var weaponRight = m_Equipment.RightHand as Weapon;
			var weaponLeft = m_Equipment.LeftHand as Weapon;

			bool twoHanded = (weaponLeft != null && weaponLeft.Size == WeaponSize.TwoHanded) || (weaponRight != null && weaponRight.Size == WeaponSize.TwoHanded);
			
			if(m_Equipment.RightHand is Weapon)
			{

				if (weaponRight.Size == WeaponSize.TwoHanded)
				{
					builder.Append(String.Format("\n\t\tBoth Hands: {0}", weaponRight.Name));
				}
				else
				{
					builder.Append(String.Format("\n\t\tRight Hand: {0}", weaponRight.Name));
				}
			}
			else if (!twoHanded)
			{
				builder.Append("\n\t\tRight Hand: Nothing.");
			}

			if(m_Equipment.LeftHand is Weapon)
			{
				if (weaponLeft.Size == WeaponSize.TwoHanded)
				{
					builder.Append(String.Format("\n\t\tBoth Hands: {0}", weaponLeft.Name));
				}
				else
				{
					builder.Append(String.Format("\n\t\tLeft Hand: {0}", weaponLeft.Name));
				}
			}
			else if (!twoHanded)
			{
				builder.Append("\n\t\tLeft Hand: Nothing.");
			}

			return builder.ToString();
		}
	}
}
