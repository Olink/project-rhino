﻿using System.Collections.Generic;
using System.IO;
using CommonUtilities.Concepts;
using CommonUtilities.Item;

namespace CommonUtilities.Unit
{
	public class EquipmentManager : ISerializable
	{

		public IEquipment Head
		{
			get { return m_Equipment[EquipmentSlot.Head]; }
			set { m_Equipment[EquipmentSlot.Head] = value; }
		}

		public IEquipment Body
		{
			get { return m_Equipment[EquipmentSlot.Armor]; }
			set { m_Equipment[EquipmentSlot.Armor] = value; }
		}

		public IEquipment Arm
		{
			get { return m_Equipment[EquipmentSlot.Glove]; }
			set { m_Equipment[EquipmentSlot.Glove] = value; }
		}

		public IEquipment Leg
		{
			get { return m_Equipment[EquipmentSlot.Boots]; }
			set { m_Equipment[EquipmentSlot.Boots] = value; }
		}

		public IEquipment Accessory
		{
			get { return m_Equipment[EquipmentSlot.Accessory]; }
			set { m_Equipment[EquipmentSlot.Accessory] = value; }
		}

		public IEquipment LeftHand
		{
			get { return m_Equipment[EquipmentSlot.LeftHand]; }
			set { m_Equipment[EquipmentSlot.LeftHand] = value; }
		}

		public IEquipment RightHand
		{
			get { return m_Equipment[EquipmentSlot.RightHand]; }
			set { m_Equipment[EquipmentSlot.RightHand] = value; }
		}

		private Dictionary<EquipmentSlot, IEquipment> m_Equipment;
		public EquipmentManager()
		{
			m_Equipment = new Dictionary<EquipmentSlot, IEquipment>();
			m_Equipment[EquipmentSlot.Head] = new EmptyEquipment(EquipmentSlot.Head);
			m_Equipment[EquipmentSlot.Armor] = new EmptyEquipment(EquipmentSlot.Armor);
			m_Equipment[EquipmentSlot.Boots] = new EmptyEquipment(EquipmentSlot.Boots);
			m_Equipment[EquipmentSlot.Glove] = new EmptyEquipment(EquipmentSlot.Glove);
			m_Equipment[EquipmentSlot.Accessory] = new EmptyEquipment(EquipmentSlot.Accessory);
			m_Equipment[EquipmentSlot.RightHand] = new EmptyEquipment(EquipmentSlot.RightHand);
			m_Equipment[EquipmentSlot.LeftHand] = new EmptyEquipment(EquipmentSlot.LeftHand);
		}

		public IEquipment GetEquipment(EquipmentSlot slot)
		{
			return m_Equipment[slot];
		}

		public void EquipItem(IEquipment e, EquipmentSlot slot)
		{
			m_Equipment[slot] = e;
		}

		public IEquipment RemoveItem(EquipmentSlot slot)
		{
			var item = GetEquipment(slot);
			m_Equipment[slot] = new EmptyEquipment(slot);
			return item;
		}

		public void Write(Stream s)
		{
			Head.Write(s);
			Body.Write(s);
			Leg.Write(s);
			Arm.Write(s);
			Accessory.Write(s);
			RightHand.Write(s);
			LeftHand.Write(s);
		}

		public void Read(Stream s)
		{
			m_Equipment[EquipmentSlot.Head] = ItemFactory.GetEquipment(s);
			m_Equipment[EquipmentSlot.Armor] = ItemFactory.GetEquipment(s);
			m_Equipment[EquipmentSlot.Boots] = ItemFactory.GetEquipment(s);
			m_Equipment[EquipmentSlot.Glove] = ItemFactory.GetEquipment(s);
			m_Equipment[EquipmentSlot.Accessory] = ItemFactory.GetEquipment(s);
			m_Equipment[EquipmentSlot.RightHand] = ItemFactory.GetEquipment(s);
			m_Equipment[EquipmentSlot.LeftHand] = ItemFactory.GetEquipment(s);
		}
	}
}
